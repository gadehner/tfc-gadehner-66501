export class Document {
  constructor(_id= '', title='', cuerpo='', trazabilidad='', calificacion=0, type='', username='', relevancia='', parametro_de_busqueda='', posicion='', method=0){
    this.posicion=posicion;
    this._id =_id;
    this.title = title;
    this.cuerpo = cuerpo;
    this.trazabilidad = trazabilidad;
    this.calificacion = calificacion;
    this.type = type;
    this.method = method;
    this.username = username;
    this.relevancia = relevancia;
    this.parametro_de_busqueda =parametro_de_busqueda;
    
  }
  posicion:string;
  _id: string;
  title: string;
  cuerpo: string;
  trazabilidad: string;
  calificacion: number;
  type: string;
  username: string;
  relevancia:string;
  parametro_de_busqueda:string;
  method: number;

}
