export class DataTest {
    precision : number;
    exhaustividad : number;
    fscore : number;
    relevantes_recuperados_correctamente : number;
    total_relevantes_recuperados : number;
    total_relevantes : number;
    username : string;
    query:string;
}
