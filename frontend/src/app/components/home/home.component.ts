import { Component, OnInit } from '@angular/core';
//import { DataApiService } from 'src/app/services/data-api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(/*private dataApi: DataApiService*/private router: Router) { }

  ngOnInit() {
    /*if(this.credenciales()){
      this.router.navigate(['user/login']);
    }*/
    this.credenciales();
    
  }
  credenciales(){
    //localStorage.removeItem('auth_token')
    //console.log(localStorage.getItem('auth_token'));
    //console.log(localStorage.getItem('currentUser'));
    
    if(String(localStorage.getItem('auth_token')).length>4){
      //console.log('no null');
      return true;
    }else{
      //console.log('null');
      this.router.navigate(['../user/login']);
      return false;
    }
  }
  

}
