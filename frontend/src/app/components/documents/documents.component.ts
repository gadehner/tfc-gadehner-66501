import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import {DocumentsService} from '../../services/documents.service';
import {TestService} from '../../services/test.service'
import {NgForm} from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import {Document} from '../../models/document';
import { isNullOrUndefined } from 'util';
import { Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
//import {DialogDetailsComponent} from './dialog-details/dialog-details.component'
import {MatTooltipModule} from '@angular/material/tooltip';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { DataTest } from 'src/app/models/data-test';
import { BehaviorTest } from 'src/app/models/behavior-test';




// para usar materialize
declare var M: any;



@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css'],
  providers: [DocumentsService]
})



export class DocumentsComponent implements OnInit {
  displayedColumns: string[] = ['posicion', 'no_expediente', 'no_resolucion', 'anio', 
                                'resolucion', 'grado_relevancia', 'explicacion', 'evaluacion'];
  dataSource = null;
  valor_maximo = 0;
  valor_minimo = 0;
  dimension_intervalo = 0;
  CANTIDAD_INTERVALOS = 5;
  valores_discretos: string[] = ['Nada Relevante','Poco Relevante',
                                'Suficientemente Relevante','Relevante','Muy Relevante'];
  color_relevancia= 'undefinded';
  CANTIDAD_SENTENCIAS = 10;
  ID_CORPUS: number=0;
  selectedValue: string = 'buscar';
  selectedValue2: string;

  acciones: Select[] = [
    {value: 'buscar', viewValue: 'Búsqueda de Sentencias'},
    {value: 'prueba', viewValue: 'Prueba de Estrategia'},

  ];
  tipos_prueba: Select[];
  pruebas_almacenadas: Select[] = [
    {value: 'uno', viewValue: 'Prueba 1'},
    {value: 'dos', viewValue: 'Prueba 2'},

  ];
  metricas: SelectMetricas[] = [
    {value: 'precision', viewValue: 'Precisión', pointValue:0},
    {value: 'exhaustividad', viewValue: 'Exhaustividad', pointValue:1},
    {value: 'fscore', viewValue: 'F-Score', pointValue:2},

  ];
  panelOpenState =true;
  panelOpenState2 =true;
  checked_almacenar = false;
  contador_evaluados = 0;
  nombre_prueba='';

  precision = 0;
  exhaustividad = 0;
  fscore = 0;
  relevantes_recuperados_correctamente = 0;
  total_relevantes_recuperados = 0;
  total_relevantes = 0;
  data_test :DataTest;
  behavior :BehaviorTest;
  checked_bsin=false;
  checked_bsemngd=false;
  checked_bsemnld=false;
  checked_concluster=false;
  checked_sincluster=false;
  checked_tnoautoma = false;
  checked_tautoma = false;
  number_iteraciones;




  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  
  
  provisorio = false;
  habilitar_filtro = false;
  constructor(private documentService: DocumentsService, private authService: AuthService,
    private router: Router, public dialog: MatDialog, private testService : TestService) { }

  ngOnInit() {
    //this.credenciales();
    if(this.authService.getTest){
      this.testService.getCorpusDataTest()
      .subscribe(res => {
        this.tipos_prueba=[];
        this.tipos_prueba = res as Select[];
        //console.log(res);
      });
      //Cuando selecciona una, obtener id y setear cantidad de sentencias...


    }
    
  }
  addDocument(form?: NgForm) {
    console.log(form.value);
    if (form.value._id) {
      this.documentService.putDocument(form.value)
        .subscribe(res => {
          this.resetForm(form);
          this.getDocuments();
          M.toast({html: 'Actualizado correctamente'});
        });
    } else {
      this.documentService.postDocument(form.value)
        .subscribe(res => {
          this.getDocuments();
          this.resetForm(form);
          M.toast({html: 'Guardado correctamente'});
        });
    }

  }
  getDocuments() {
    this.documentService.getDocuments()
      .subscribe(res => {
        this.documentService.documents = res as Document[];
        console.log(res);
      });
  }
  editDocument(document: Document) {
    this.documentService.selectedDocument = document;
  }
  searchDocument(paramSearch: string) {
    this.provisorio = true;
    this.habilitar_filtro = false;
    console.log(paramSearch);
    

    this.documentService.getDocument(paramSearch)
      .subscribe(res => {
        
        this.documentService.documents = res as Document[]; 
        if(this.documentService.documents[0].title!=null){
          
        
      // M.toast({html: this.documentService.documents});
      // this.resetForm();
          
          this.dataSource = new MatTableDataSource(this.documentService.documents);
          this.dataSource.paginator = this.paginator;
          this.habilitar_filtro = true;
          this.establecer_valores_relevancia();
        }else{
          confirm('No se encontraron resultados con el termino ingresado.');
          
        }
        this.provisorio = false;
    
      console.log(res);
    });
    // router.get('/:title', document.searchDocument)
  }

  testModel(paramSearch: string) {
    //ver si es una prueba o si es una busqueda normal
  
    //alert(this.checked_tautoma);
    //alert(this.number_iteraciones);

    this.provisorio = true;
    this.habilitar_filtro = false;
    //console.log(paramSearch);
    console.log('testMod');
    var method=-1;
    if(this.checked_bsin){
      if(this.checked_sincluster){
        method = 1; //busqueda sintactica
      }else{
        method = 6; //busqueda sintactica con clustering
      }
    }else{
      if(this.checked_bsemngd){
        if(this.checked_sincluster){
          method = 2; //busqueda semantica con ngd, sin clustering
        }else{
          method = 3; //busqueda semantica con ngd, con clustering
        }
      }else{
        if(this.checked_bsemnld){ 
          if(this.checked_sincluster){
            method = 4; //busqueda semantica con distancia local, sin clustering
          }else{
            method = 5; //busqueda semantica con distancia local, con clustering
          }
        }

      }

    }

    console.log(method);
    var id_corpus;
    if(localStorage.getItem('test')===null){
      id_corpus = null;
    
    }else{
      id_corpus = this.ID_CORPUS;
    }
    console.log(id_corpus);
    
    //console.log(this.CANTIDAD_SENTENCIAS);
    if(this.checked_tnoautoma){
      this.number_iteraciones = 'undefined';
    }else{
      this.number_iteraciones = parseInt(this.number_iteraciones);
    }
    //while(this.number_iteraciones!==0){
      this.documentService.testModel(paramSearch, id_corpus, method, this.number_iteraciones)
        .subscribe(res => {
          
          this.documentService.documents = res as Document[]; 
          if(this.documentService.documents[0].title!=null){
            
          
        // M.toast({html: this.documentService.documents});
        // this.resetForm();
            
            this.dataSource = new MatTableDataSource(this.documentService.documents);
            this.dataSource.paginator = this.paginator;
            this.habilitar_filtro = true;
            this.establecer_valores_relevancia();
          }else{
            confirm('No se encontraron resultados con el termino ingresado.');
            
          }
         // if(this.checked_tnoautoma){
            this.provisorio = false;
          //}
      
        //this.habilitar_filtro = true;
        //this.provisorio = false;
        this.number_iteraciones = this.number_iteraciones-1;

        console.log(res);
        if(this.number_iteraciones!=='undefined' && this.number_iteraciones > 0){
          this.testModel(paramSearch);
        }

      });
      
   // }
    //this.provisorio = false;

    // router.get('/:title', document.searchDocument)
    
  }






  deleteDocument(_id: string) {
    if (confirm('Estas seguro de querer eliminarlo?')) {
      this.documentService.deleteDocument(_id)
        .subscribe(res => {
          this.getDocuments();
          M.toast({html: 'Eliminado satisfactoriamente'});
        });
    }
  }

  details(document: Document) {
    const dialogRef = this.dialog.open(DialogDetails, {
      width: '750px',
      data: {document: document}
      
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('Cerrar Dialogo');
     
    });
      
    
  }
 
  feedback(document: Document, type: string) {
    
    //if(confirm('Seguro? ')){//sacar esto, es re denso...
      
     
        document.calificacion=0;
        document.type = type;
        var usuario = localStorage.getItem('currentUser');
        //console.log(usuario)
        usuario = JSON.parse(usuario);
        document.username = usuario['username'];
      
        console.log(document);

        this.contador_evaluados= this.contador_evaluados + 1;
        console.log(this.contador_evaluados);
        var method = this.getMethod();
        document.method = method;
        if(localStorage.getItem('test')===null /*&& this.checked_almacenar==false*/){
          


          this.documentService.postDocuments(document)
            .subscribe(res => {
              console.log(res);
              
              /*this.documentService.documents = res as Document[]; // tengo que depurar esto
            // M.toast({html: this.documentService.documents});
            // this.resetForm();
            console.log(res);*/
          });
        }else{
          if(localStorage.getItem('test')!==null){
            var valor_index_sistema = this.obtener_valor_index(document.relevancia);
            var valor_experto = this.obtener_valor_index(document.type);
            //console.log(valor_index_sistema);
            //console.log(valor_experto);
            if(valor_index_sistema == valor_experto){
              if(valor_index_sistema ==1){
                this.relevantes_recuperados_correctamente = this.relevantes_recuperados_correctamente + 1;
                this.total_relevantes_recuperados = this.total_relevantes_recuperados + 1;
                this.total_relevantes = this.total_relevantes + 1;
              }
            }else{
              if(valor_index_sistema==1){
                this.total_relevantes_recuperados = this.total_relevantes_recuperados + 1;
              }else{
                if(valor_experto==1){
                  this.total_relevantes = this.total_relevantes + 1;
                }
              }
            }
            this.setearMetricas();
            this.behavior = new BehaviorTest();
            this.behavior.position = document.posicion; 
            this.behavior.relevance_ant = document.relevancia;
            this.behavior.relevance_post = document.type;
            this.behavior.username = usuario['username'];
            this.testService.registerBehavior(this.behavior).subscribe(res =>{
                console.log(res);
            });
            this.documentService.postDocuments(document)
              .subscribe(res => {
                console.log(res);
              });

            if(this.contador_evaluados==this.CANTIDAD_SENTENCIAS){
              this.data_test = new DataTest();
              this.data_test.exhaustividad = this.exhaustividad;
              this.data_test.fscore = this.fscore;
              this.data_test.precision = this.precision;
              this.data_test.relevantes_recuperados_correctamente = this.relevantes_recuperados_correctamente;
              this.data_test.total_relevantes = this.total_relevantes;
              this.data_test.total_relevantes_recuperados = this.total_relevantes_recuperados;
              this.data_test.username = usuario['username'];
              //this.data_test.name_test = this.nombre_prueba;
              this.data_test.query = document.parametro_de_busqueda;

              this.testService.registerDataTest(this.data_test).subscribe(res => {
                
                console.log(res);
                M.toast({html: 'Prueba guardada correctamente'});
                
                /*this.documentService.documents = res as Document[]; // tengo que depurar esto
              // M.toast({html: this.documentService.documents});
              // this.resetForm();
              console.log(res);*/
            });
            }
          }
        } 
        
    //}
    // router.get('/:title', document.searchDocument)
  }
  setearMetricas(){
    this.precision = this.relevantes_recuperados_correctamente/this.total_relevantes_recuperados;
    this.exhaustividad = this.relevantes_recuperados_correctamente/this.total_relevantes;
    this.fscore = 2*((this.precision*this.exhaustividad)/(this.precision+this.exhaustividad))
  }
  obtener_valor_index(relevancia){
    var index=-1;
    for(var i=0; i<this.valores_discretos.length;i++){
      if(relevancia==this.valores_discretos[i]){
        index = i;
      }
    }
    if(index > 0){
      index=1;
    }
    return index;
  }
  credenciales(){
    //localStorage.removeItem('auth_token')
    //console.log(localStorage.getItem('auth_token'));
    //console.log(localStorage.getItem('currentUser'));
    
    if(String(localStorage.getItem('auth_token')).length>4){
      //console.log('no null');
      return true;
    }else{
      //console.log('null');
      this.router.navigate(['../user/login']);
      return false;
    }
  }

  getMethod(){
    var method=-1;
    if(this.checked_bsin){
      if(this.checked_sincluster){
        method = 1; //busqueda sintactica
      }else{
        method = 6; //busqueda sintactica con clustering
      }
    }else{
      if(this.checked_bsemngd){
        if(this.checked_sincluster){
          method = 2; //busqueda semantica con ngd, sin clustering
        }else{
          method = 3; //busqueda semantica con ngd, con clustering
        }
      }else{
        if(this.checked_bsemnld){ 
          if(this.checked_sincluster){
            method = 4; //busqueda semantica con distancia local, sin clustering
          }else{
            method = 5; //busqueda semantica con distancia local, con clustering
          }
        }

      }

    }
    console.log(method);
    return method;
  }


  logout(){
    localStorage.removeItem('auth_token');
    localStorage.removeItem('currentUser');
    this.router.navigate(['../user/login']);
  }
 


  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.documentService.selectedDocument = new Document();
    }

  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  establecer_valores_relevancia(){
    this.valor_maximo = this.dataSource.filteredData[0].ranking;
    if(this.dataSource.filteredData.length==this.CANTIDAD_SENTENCIAS){
      this.valor_minimo = this.dataSource.filteredData[this.CANTIDAD_SENTENCIAS-1].ranking;
    }else{
      this.valor_minimo = this.dataSource.filteredData[this.dataSource.filteredData.length-1].ranking;
    }
    //console.log(this.dataSource.filteredData[0].ranking);
    this.dimension_intervalo = (this.valor_maximo - this.valor_minimo)/this.CANTIDAD_INTERVALOS;
  }


  obtener_relevancia(valor_ranking: Number){
    var index=-1;
    for(var i=0; i < 5; i++){
      if(valor_ranking>=(this.dimension_intervalo * i) && valor_ranking<=(this.dimension_intervalo * (i+1)) ){
        index = i;
      }
    }
    if(index==-1){
      index=4;
    }
      
    return this.valores_discretos[index];
  }

 
  public getColor(relevancia: string): string{
    var index=-1;
    for(var i=0; i < 5; i++){
      if(relevancia == this.valores_discretos[i] ){
        index = i;
      }
    }
    
    var color = ['crimson','chocolate','coral','rgb(176, 235, 117)','rgb(83, 231, 231)'];   
    return color[index];
 }
 changeCorpus(id : String){
    //console.log(id);
    this.ID_CORPUS = Number(id);
    this.testService.getQuantitySentencias(id)
    .subscribe(res => {
      this.CANTIDAD_SENTENCIAS = res[0].quantity as number;
      //console.log(this.CANTIDAD_SENTENCIAS);
    });
    
    
 }


}
@Component({
  selector: 'dialog-details.component',
  templateUrl: 'dialog-details.component.html',
})
export class DialogDetails {
  panelOpenState = false;
  constructor(
    public dialogRef: MatDialogRef<DialogDetails>,
    @Inject(MAT_DIALOG_DATA) public data: Document) {}

  onNoClick(): void {
    this.dialogRef.close();
  }


}
export interface Select {
  value: string;
  viewValue: string;
}
export interface SelectMetricas {
  value: string;
  viewValue: string;
  pointValue: number;
}

