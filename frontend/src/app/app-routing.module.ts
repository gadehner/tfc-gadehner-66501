import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/user/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { Page404Component } from './components/page404/page404.component';
import { DocumentsComponent } from './components/documents/documents.component';
import { RegisterComponent } from './components/user/register/register.component';
import { UserComponent } from './components/user/user.component';
import { TabsComponent } from './components/tabs/tabs.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  
  //{path: 'offers', component:OffersComponent}, //solo us autentificados
  //{path: 'book/:id', component: DetailsBooksComponent},
  //{path: 'admin/list-books', component: ListBooksComponent},//solo us autentificados
  
  {path: 'documents', component: DocumentsComponent},
  {path: 'home', component: HomeComponent},
  {path: 'tabs', component: TabsComponent},
  
  //{path: 'user/login', component: LoginComponent},
  //{path: 'user/register', component: RegisterComponent},
  {path: 'user/profile', component: ProfileComponent},//solo us autentificados
  {
    path: 'user/signup', component: UserComponent,
    children: [{ path: '', component: RegisterComponent }]
  },
  {
      path: 'user/login', component: UserComponent,
      children: [{ path: '', component: LoginComponent }]
  },
  {path: '**', component: Page404Component}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
