import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DocumentsComponent } from './components/documents/documents.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/user/login/login.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { Page404Component } from './components/page404/page404.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RegisterComponent } from './components/user/register/register.component';
import { MatTabsModule, MatButtonModule, MatFormFieldModule, MatInputModule, MatRippleModule, MatTooltipModule, MatExpansionModule, MatProgressBarModule, MatListModule, MatTableModule, MatPaginatorModule, MatSelectModule, MatCheckbox, MatCheckboxModule, MatCardModule, MatIconModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDialogModule} from '@angular/material/dialog';



//services
import { DocumentsService } from './services/documents.service';
import { UserService } from './services/user.service';
import { TestService } from './services/test.service';
import { AuthService } from './services/auth.service';

//para formularios
import {FormsModule} from "@angular/forms";

//para http cliente
import {HttpClientModule} from "@angular/common/http";
import { UserComponent } from './components/user/user.component';
import { TabsComponent } from './components/tabs/tabs.component';
//import { DialogDetailsComponent } from './components/documents/dialog-details/dialog-details.component';

//para Details Diali
import {DialogDetails} from './components/documents/documents.component'



@NgModule({
  declarations: [
    AppComponent,
    DocumentsComponent,
    HomeComponent,
    LoginComponent,
    ProfileComponent,
    Page404Component,
    NavbarComponent,
    UserComponent,
    RegisterComponent,
    TabsComponent,
    //DialogDetailsComponent
    DialogDetails,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatTabsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatDialogModule,
    MatTooltipModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    MatCheckboxModule,
    MatCardModule,
    MatIconModule
    

  ],
  exports: [
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatTooltipModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    MatCheckboxModule,
    MatCardModule,
    MatIconModule
  ],
  providers: [UserService,  AuthService, TestService],
  bootstrap: [AppComponent],
  entryComponents: [DialogDetails]
})
export class AppModule { }
