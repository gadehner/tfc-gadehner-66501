import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import { Response } from "@angular/http";
//import {Observable} from 'rxjs';
//import 'rxjs/add/operator/map';
import { User } from '../models/user';
import {config} from '../config/config';
 
@Injectable()
export class UserService {
  
  readonly URL_API = 'http://'+config.IP+':'+config.PORT_BACKEND+'/api';
  constructor(private http: HttpClient) { }
 
  registerUser(user : User){
    const body: User = {
      username: user.username,
      password: user.password,
      email: user.email,
      firstname: user.firstname,
      lastname: user.lastname
    }
    console.log(user);
    //return this.http.post(this.URL_API + '/crear/', user)
    return this.http.post(this.URL_API + '/user/crear', user);
    
       
  }
    
    //return this.http.post(this.rootUrl + '/api/User/Register', body);
    /*console.log('llego');
    console.log(body);
    return body;*/
  
 
}