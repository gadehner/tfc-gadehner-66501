import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataTest } from '../models/data-test';
import {config} from '../config/config';
import { BehaviorTest } from '../models/behavior-test';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  readonly URL_API = 'http://'+config.IP+':'+config.PORT_BACKEND+'/api';
  constructor(private http: HttpClient) { }
 
  registerDataTest(data_test : DataTest){
    /*const body: User = {
      username: user.username,
      password: user.password,
      email: user.email,
      firstname: user.firstname,
      lastname: user.lastname
    }*/
    //console.log(user);
    //return this.http.post(this.URL_API + '/crear/', user)
    return this.http.post(this.URL_API + '/datatest/crear', data_test);
    
       
  }
  registerBehavior(behavior : BehaviorTest){
    
    return this.http.post(this.URL_API + '/datatest/crearbehavior', behavior);
    
       
  }

  getCorpusDataTest() {
    return this.http.get(this.URL_API+'/datatest');
  }
  getQuantitySentencias(id:String) {
    return this.http.get(this.URL_API+'/datatest'+`/${id}`);
  }

}
