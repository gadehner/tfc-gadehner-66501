import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { isNullOrUndefined } from 'util';
import {config} from '../config/config';
declare var M: any;

@Injectable({
  providedIn: 'root'
})
 
export class AuthService {
 
  uri = 'http://'+config.IP+':'+config.PORT_BACKEND+'/api';
  token;

  constructor(private http: HttpClient,private router: Router) { }
  login(username: string, password: string) {
    this.http.post(this.uri + '/user', {username: username,password: password})
    .subscribe((resp: any) => {
     console.log(resp);
      if(resp.status === 'OK'){
        localStorage.setItem('auth_token', resp.token);
        
        let user_string = JSON.stringify({username, password});
        localStorage.setItem('currentUser', user_string);

        this.router.navigate(['documents']);
        
      }else{
        console.log('M: error. '+resp);
        M.toast({html: 'Usuario y/o contraseña incorrecta'});
      }
      //M.toast({html: resp.status});
      
    });
       
    }
    /*logout() {

      localStorage.removeItem('auth_token');
      //console.log(localStorage.getItem('auth_token'));
    }*/
   
    public getlogIn(): boolean {
      return (localStorage.getItem('auth_token') !== null);
    }
    logout(){
      localStorage.removeItem('auth_token');
      localStorage.removeItem('currentUser');
      localStorage.removeItem('test');
      this.router.navigate(['../user/login']);
    }
    setTest(condicion_prueba){
      if(condicion_prueba){
        localStorage.setItem('test', 'true');
      }else{
        localStorage.removeItem('test');
      }
    }
    getTest(){
      return (localStorage.getItem('test')!==null)
    }

    /*getToken(){
      return localStorage.getItem('auth_token');
      
    }*/

    ////
    /*setUser(user): void{
      let user_string = JSON.stringify(user);
      localStorage.setItem('currentUser', user_string);
    }
    setToken(token): void{
      localStorage.setItem('accessToken', token);
    }
  
    
  
    getCurrentUser(){
      let user_string = localStorage.getItem('currentUser');
      let user = null;
      if(!isNullOrUndefined(user_string)){
        user = JSON.parse(user_string)
      }
      return user;
    }
    logoutUser(){
      let accessToken = localStorage.getItem('accessToken');
      localStorage.removeItem('accessToken');
      localStorage.removeItem('currentUser');
    }*/
 
}