import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { Document } from "../models/document";

//import { throwError as observableThrowError,  Observable } from 'rxjs';

import { throwError ,  Observable, timer } from 'rxjs';

//import { timeout } from 'rxjs/operators';
import { 
  timeout,
  retryWhen,
  take,
  concat,
  share,
  delayWhen
} from 'rxjs/operators';

import {config} from '../config/config';




@Injectable({
  providedIn: 'root'
})
export class DocumentsService {
  selectedDocument: Document;
  documents: Document[];
  

  readonly URL_API = 'http://'+config.IP+':'+config.PORT_BACKEND+'/api/documents'; //http://localhost:3000/api/documents //http://181.230.99.69:3000/api/documents

  constructor(private http: HttpClient) {
    this.selectedDocument = new Document();
    
  }

  postDocument(document: Document) {
    return this.http.post(this.URL_API, document);
  }

  postDocuments(document: Document) {
    return this.http.post(this.URL_API + `/${document}`, document);
  }

  getDocuments() {
    return this.http.get(this.URL_API);
  }
  getDocument(paramSearch: string) {
    return this.http.get(this.URL_API + `/${paramSearch}`).pipe(timeout(300000));
    /*.pipe(
      timeout(240000),
      retryWhen(errors =>
        errors.pipe(
          delayWhen(val => timer(val * 1000))
        )
      ),
      take(2),
      concat(throwError('This is an error!')),
      share()
    );*/
    
    //pipe(timeout(3000, new Error('timeout exceeded')));



    
  }
  

  testModel(paramSearch: string, quantitySentencias_id: number, method: number, iteracion: number) {
    /*var datos_inicio_prueba : Object={
      parametro: paramSearch,
      quantity : quantitySentencias
    };*/
    var params = paramSearch + '&'+quantitySentencias_id + '&' + method + '&' + iteracion;

    return this.http.put(this.URL_API + `/${params}`, params);
  }

  putDocument(document: Document) {
    return this.http.put(this.URL_API + `/${document._id}`, document);
  }
  

  deleteDocument(_id: string) {
    return this.http.delete(this.URL_API + `/${_id}`);
  }
}
