# Modelo de Recuperación de Información Jurídica basado en ontologías y distancias semánticas.
## Trabajo final de carrera - Ingeniería en Informática.
###### Gabriel Andrés Dehner.
###### Matrícula: 66501. 
###### Directores: Ing. Héctor J. Ruidías - Mgter. Karina B. Eckert.
###### Universidad Gastón Dachary.

## Requerimientos
###### Mysql - Obtener la bd en directorio "bd-volcado/mysql" e importarla para el funcionamiento.
###### Stardog - Descargar entorno de sitio oficial https://www.stardog.com/. Luego importar las bds del directorio "bd-volcado/stardog", con los nombres correspondientes.
###### Node y npm.
###### Mongo- no es parte del TFC pero permite la autenticación para los logins en el sistema.

## Pasos para la Instalación
###### Dentro de la carpeta clonada, ejecutar en consola "npm i".
###### Dentro de la carpeta "frontend", ejecutar en consola "npm i".

## Pasos para la Ejecución
###### Dentro de la carpeta clonada, ejecutar en consola "npm run dev".
###### Dentro de la carpeta "frontend", ejecutar en consola "ng serve".
###### Seguidamente, dentro del navegador en la url "localhost:4200" se podrá visualizar el sistema.
###### Cabe mencionar que si si cambian los puertos de ejecución del frontend y backend, también deberán modificarse los archivos de configuración.



