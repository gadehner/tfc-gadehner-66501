const express = require('express');
const app = express(); //funcionalidad de la app en variable
const morgan = require('morgan');
const { mongoose } = require('./database');
const cors = require('cors');
const config = require('./config')
//var bodyParser = require('body-parser'); //para setear un mayor tamanio para pasaje de datos
/**
 * app.use (bodyParser.json ({limit: '10mb', extended: true})) 
app.use (bodyParser.urlencoded ({limit: '10mb', extended: true}))
 */




//Configuraciones
app.set('port', process.env.PORT || config.PORT_BACKEND);



//Funciones para procesar datos
app.use(morgan('dev'));//para ver las peticiones en consola
app.use((express.json()));//configurar dato json
//desp si uso angular lo de cors
app.use(cors({origin: 'http://'+config.IP+':'+config.PORT_FRONTEND})); //'http://localhost:4200'...http://181.230.99.69:4200


//Para mayor carga de datos
/*app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));*/


var bodyParser = require('body-parser');
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));


//Rutas del servidor - routes
app.use('/api/documents', require('./routes/document.routes'));
app.use('/api/user', require('./routes/user.routes'));
app.use('/api/datatest', require('./routes/data-test.routes'));

//para iniciar escucha del servidor
var server = app.listen(app.get('port'), ()=>{
   console.log('Server iniciado en puerto '+app.get('port'));
   //console.log('lmite' + bodyParser.json);
}).setTimeout(1000 * 60 * 10);

server.timeout = config.TIME_OUT;

