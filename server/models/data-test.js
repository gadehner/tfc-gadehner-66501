'use strict'

const mongoose = require('mongoose')
const {Schema} = mongoose;


const DataTestSchema = new Schema({
  
 
  precision : Number,
  exhaustividad : Number,
  fscore : Number,
  relevantes_recuperados_correctamente : Number,
  total_relevantes_recuperados : Number,
  total_relevantes : Number,
  username : String,
  name_test : String,
  query : String
})

module.exports = mongoose.model('DataTest', DataTestSchema)