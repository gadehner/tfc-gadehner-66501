const mongoose = require('mongoose');
const {Schema} = mongoose;

const TermBehaviorSchema = new Schema({
    term: {type: String, required: true},
    ponderacion: {type: Number, required:true},
    date: {type: String, required:true},
    username: {type: String, required:true},
});


//pasar a modelo de datos mongoose
module.exports = mongoose.model('TermBehavior', TermBehaviorSchema);