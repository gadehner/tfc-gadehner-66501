'use strict'

const mongoose = require('mongoose')
const {Schema} = mongoose;


const BehaviorTestSchema = new Schema({
  position : String,
  relevance_ant : String,
  relevance_post : String,
  username : String,
  date: String

})

module.exports = mongoose.model('BehaviorTest', BehaviorTestSchema)