const mongoose = require('mongoose');
//para definir esquemas de datos
const {Schema} = mongoose;

const DocumentSchema = new Schema({
    title: {type: String, required: true},
    cuerpo: {type: String, required: true}
});

//pasar a modelo de datos mongoose
module.exports = mongoose.model('Document', DocumentSchema);