var crs = {};

/**
 * Interconeccion con modulos
 */
var recup_sentencias = require('./../services/recup_sentencias');
var construc_ranking = require('./../services/construc_ranking');
var clasificacion_sentencias = require('./../services/clasificacion_sentencias');
var document_controller = require('./document.controller');
var shared_variables = require('./../services/shared-variables');
var feedback = require('./../services/feedback');
var mysql = require('./../services/shared_functions/consulta_mysql');


crs.controlador_recuperacion_informacion = async (conjunto_auxiliar, quantity_id, auxPropiedades,palabras_consulta,objetoApis, res, method, parametro_de_busqueda, numero_iteraciones) => {
    //1 modulo RECUPERAR SENTENCIAS



//ESTE MODULO CAMBIA RESPECTO A LAS PRUEBAS O FUNCIONAMIENTO GRAL
//if quantity es == null, entonces otro metodo para busquedas de sentencias  


    if(quantity_id !== null){/**conjunto auxiliar va con [0] pq es un arreglo de arreglo 
        por procesamiento de metodos anteriores.*/
        /**YA ESTA LEMATIZADO */
        var rta = await recup_sentencias.buscarSentencias_test(conjunto_auxiliar[0], quantity_id);
    }else{
        var rta = await recup_sentencias.buscarSentencias(conjunto_auxiliar[0]);
    }
    
    //armar ranking function
    var auxArr = [];
    //2 modulo CONFECCION RANKING
    auxArr = construc_ranking.confeccionar_ranking(rta.resultados, auxPropiedades, palabras_consulta, objetoApis, method);
    
    
    auxArr.sort((a, b)=> b.ranking - a.ranking);
    var arrayFormateado = [];
    //3 modulo CLASIFICACION DE SENTENCIAS 
    if(auxArr.length>0){
        if(method == 3 || method == 5){
            arrayFormateado = clasificacion_sentencias.discretizar_relevancia_test_con_clustering(auxArr, arrayFormateado, auxArr.length);
        }
        if(method == 2 || method == 4){
            arrayFormateado = clasificacion_sentencias.discretizar_relevancia_test_sin_clustering(auxArr, arrayFormateado, auxArr.length);
        }
    }
    //validar errores para que parar animaciones de busqueda en frontend...
    arrayFormateado = validar_errores(arrayFormateado, parametro_de_busqueda);
    /*for(var i=0; i<arrayFormateado.length;i++){
        console.log(arrayFormateado[i].ranking);
    }*/


    if(!(numero_iteraciones == 'undefined' || numero_iteraciones=='NaN') && numero_iteraciones!==0){
        var precision=0;
        var exhaustividad=0;
        var fscore=0;
        var relevantes_recuperados_correctamente=0;
        var total_relevantes_recuperados=0;
        var total_relevantes=0;
        for(var i=0; i<arrayFormateado.length;i++){
            
            var valor_clasif_sistema = normalizar_valor_relevancia_metricas(arrayFormateado[i].relevancia);
            var valor_clasif_expertos = normalizar_valor_relevancia_metricas(arrayFormateado[i].descripcion_rel);
           
            //trazabilidad para documentos, ranking para ver la incidencia en el dto
            //si son diferentes si feedback, sino no..

            if(!(valor_clasif_sistema == valor_clasif_expertos)){

                await feedback.feedback_in_test(arrayFormateado[i].relevancia, arrayFormateado[i].descripcion_rel, method, arrayFormateado[i].trazabilidad, arrayFormateado[i].parametro_de_busqueda, arrayFormateado[i].ranking_sin_credito);
                console.log("CLASIFICO INCORRECTAMENTE....");
                //poner lo de base de dato sparql test
            }else{
                console.log("CLASIFICO CORRECTAMENTE....");
            }
            

             if(valor_clasif_sistema == valor_clasif_expertos){
              if(valor_clasif_sistema ==1){
                relevantes_recuperados_correctamente = relevantes_recuperados_correctamente + 1;
                total_relevantes_recuperados = total_relevantes_recuperados + 1;
                total_relevantes = total_relevantes + 1;
              }
            }else{//aca tmb se podria poner lo de feedback
              if(valor_clasif_sistema==1){
                total_relevantes_recuperados = total_relevantes_recuperados + 1;
              }else{
                if(valor_clasif_expertos==1){
                  total_relevantes = total_relevantes + 1;
                }
              }
            }
            /*if(valor_clasif_sistema == valor_clasif_expertos){
                console.log('igual');
                
            }
            
            console.log(arrayFormateado[i].descripcion_rel+'  '+arrayFormateado[i].relevancia);*/
        }
        precision = relevantes_recuperados_correctamente/total_relevantes_recuperados;
        exhaustividad = relevantes_recuperados_correctamente/total_relevantes;
        fscore = 2*((precision*exhaustividad)/(precision+exhaustividad));
        console.log("Precision: " + precision);
        console.log("Exhaustividad: " + exhaustividad);
        console.log("Fscore: " + fscore);
        var csrrcytot = ' '+ relevantes_recuperados_correctamente + ' y '+ total_relevantes_recuperados;
        console.log('numero de iteraciones: '+numero_iteraciones);
        console.log('parametro: '+parametro_de_busqueda);
       //aca llamada a sql, id_corpus, numero autoincrementable de prueba, valores p, e, f, numero_iteracion
        mysql.consultaMysql('INSERT INTO resultados_test (id_test, id_method, numero_iteracion, consulta_busqueda, precis, exhaustividad, fsc, csrrcytot)'
                          + ' VALUES ('+quantity_id+','+method+','+numero_iteraciones+",'"+parametro_de_busqueda+"',"+precision+','+exhaustividad+','+fscore+",'"+csrrcytot+"')");
        

    }//mandar req y res....
    //verificar si es una prueba o no
    //document_controller.feedback //req y res
    //document_controller.testModel // params
    
    //console.log(arrayFormateado[0]);
    res.json(arrayFormateado);
}

function normalizar_valor_relevancia_metricas(valor_cualitativo){
    var index = -1;
    for(var i=0; i < shared_variables.VALORES_DISCRETOS.length; i++){
        if(shared_variables.VALORES_DISCRETOS[i]==valor_cualitativo){
            index = i;
            i = shared_variables.VALORES_DISCRETOS.length;
        }
    }
    if(index==0 || index==1){
        index=0;//nada relevante
    }else{
        index=1;//relevante
    }

    return index;
}


function validar_errores(arrayFormateado, parametro_de_busqueda){
    //para verificar error arrays...
 if(!(typeof arrayFormateado[0] == 'object')){
     console.log('no');
     for(var i=0; i< 10; i++){
         var obj = new Object();                                     
         obj['trazabilidad']='asdasd'
         obj['ranking'] = 0;
         obj['calificacion'] = 'undefined';
         obj['texto'] = 'undefined';
         obj['posicion'] = i+1;
         obj['parametro_de_busqueda'] = parametro_de_busqueda;
         arrayFormateado[arrayFormateado.length]=obj;
     }
 }
 return arrayFormateado;
}

module.exports = crs;