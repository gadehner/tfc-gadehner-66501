const DataTest = require('../models/data-test'); //para consultas a BD
const BehaviorTest = require('../models/behavior-test');
const data_testController = {};
const JWT_Secret = 'your_secret_key';
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
/*
const natural = require('natural');

console.log(natural.PorterStemmerEs.stem("jugaría"));
console.log(natural.PorterStemmerEs.stem("considero"));
console.log(natural.PorterStemmerEs.stem("tomates"));
console.log(natural.PorterStemmerEs.stem("delito"));*/

/*
 Para conexion a mysql 
*/
const mysql = require('mysql');
var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: "bdres"
});

data_testController.createDataTest = async (req, res) => {
   
    const data_test = new DataTest({
        precision : req.body.precision,
        exhaustividad : req.body.exhaustividad,
        fscore : req.body.fscore,
        relevantes_recuperados_correctamente : req.body.relevantes_recuperados_correctamente,
        total_relevantes_recuperados : req.body.total_relevantes_recuperados,
        total_relevantes : req.body.total_relevantes,
        username : req.body.username,
        name_test : req.body.name_test,
        query : req.body.query
    });
    console.log(data_test);
    

   /* var usaux = await DataTest.find({name_test: data_test.name_test});
    if(usaux.length <= 0){*/
        
        await data_test.save();
        res.json({
        'status': 'Prueba guardada Correctamente'
        });
        

   /* }else{
        res.json({
            'status': 'Error: Nombre de Prueba ya existente'
        });
    }*/

};
data_testController.createBehaviorTest = async (req, res) => {
   
    const behavior = new BehaviorTest({
        position : req.body.position,
        relevance_ant : req.body.relevance_ant,
        relevance_post : req.body.relevance_post,
        username : req.body.username,
        date: Date.now()
    });
    console.log(behavior);
    
        
    await behavior.save();
    res.json({
        'status': 'Ok'
    });
        

 
};

function consultaMysql(query_, con){
    return new Promise(function(resolve, reject){
        
        con.query(query_, function (err, result, fields) {
            if (err){ console.log(err); throw err;};
            resolve({resol: result});
        });
        
    });
}

data_testController.getCorpus = async (req, res) => {
   
    var query = 'SELECT idtest as value, description as viewValue FROM `tests`';
    var resultado = (await consultaMysql(query, con)).resol;

    res.json(resultado);

};

data_testController.getQuantitySentencias = async (req, res) => {
    var  id = req.params.id;
    var query = "SELECT COUNT(*) as quantity FROM resoluciones res LEFT JOIN testsresoluciones tr"
    +" ON res.numero_expediente=tr.numero_expediente"
    +" AND res.numero_resolucion=tr.numero_resolucion" 
    +" AND res.anio_resolucion=tr.anio_resolucion WHERE tr.idtest='"+ id +"'" ;
    var resultado = (await consultaMysql(query, con)).resol;

    res.json(resultado);
}


module.exports =data_testController;


