/**
 * Para exportar modulo completo al final
 */
const cntrdr_obtencion_info_relac = {};

/**
 * Para poder hacer stemming de palabras al realizar la búsqueda  
 */ 
const natural = require('natural');

/**
 * Modulos referidos a obtencion de informacion relacionada 
 */
var procesamiento_consulta = require('./../services/procesamiento_consulta');
var expansion_consulta = require('./../services/expansion_consulta');
var calculo_dist_sem = require('./../services/calculo_distancias_semanticas');
//el de almacen local colocar, se va a consultar a stardogy mysql

var crs = require('./cntrdr_recup_sentencias');
var busqueda_literal = require('./../services/busqueda_literal');

cntrdr_obtencion_info_relac.controlador_informacion_relacionada = async (parametro_de_busqueda, res, quantity_id, method, numero_iteraciones)=>{
//(1) busqueda sintactica 
//(2) busqueda semantica con ngd, sin clustering
//(3) busqueda semantica con ngd, con clustering
//(4) busqueda semantica con distancia local, sin clustering
//(5) busqueda semantica con distancia local, con clustering
//(6) busqueda sintactica, con clustering
    var palabras_consulta = parametro_de_busqueda.split('+');

    if(method==1 || method == 6){
        busqueda_literal.busqueda_lite(palabras_consulta, res, quantity_id, method);

    }else{
        var conjuntoBusquedaSentencias=[];
        var objetoApis=[];
        for(var i = 0; i<palabras_consulta.length; i++){
            //PROCESAMIENTO DE CONSULTA
            palabras_consulta[i]= await procesamiento_consulta.adecuacion_de_termino(palabras_consulta[i]); 

            //EXPANSION DE CONSULTA
            var json = await expansion_consulta.expandir_consulta(palabras_consulta[i]);

            //arreglos para mostrar en frontend de donde proviene cada palabra relacionada
            conjuntoBusquedaSentencias[conjuntoBusquedaSentencias.length] = json.cbs;
            objetoApis[objetoApis.length] = json.oapis;
        }
        
        //arreglos que almacenan temporalmente todas las relaciones semanticas
        var auxPropiedades = []; 
        var conjunto_auxiliar=[];
        for(var i=0; i<conjuntoBusquedaSentencias.length;i++){
            conjunto_auxiliar = unir_array_objetos(conjunto_auxiliar, conjuntoBusquedaSentencias);
            
            auxPropiedades[auxPropiedades.length] = await calculo_dist_sem.obtener_distancias_semanticas_test(
                    conjuntoBusquedaSentencias[i], palabras_consulta[i], method, numero_iteraciones);
        }

    /*
    ** MODULO CONTROLADO DE RECUPERACION DE SENTENCIAS
    */    
   console.log('mando');
        await crs.controlador_recuperacion_informacion(conjunto_auxiliar, quantity_id, 
            auxPropiedades,palabras_consulta,objetoApis, res, method, parametro_de_busqueda, numero_iteraciones);
    
    }   
}

function unir_array_objetos(array_1, array_2){
    for(var i = 0; i < array_2.length; i++){
        array_1[array_1.length]=array_2[i];
    }
    return array_1;
}

 module.exports = cntrdr_obtencion_info_relac;