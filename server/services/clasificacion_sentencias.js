var clasificacion_sentencias = {};
var clusterfck = require("tayden-clusterfck");
var shared = require('./shared-variables');
const CANTIDAD_INTERVALOS = shared.CANTIDAD_INTERVALOS;
var CANTIDAD_SENTENCIAS = shared.CANTIDAD_SENTENCIAS;
const valores_discretos = shared.VALORES_DISCRETOS;

clasificacion_sentencias.discretizar_relevancia_test_con_clustering = (auxArr, arrayFormateado, cantidad_test) => {
    CANTIDAD_SENTENCIAS = cantidad_test;
    /*var valor_maximo = auxArr[0].ranking;
    
    if(auxArr.length>10){
        var valor_minimo = auxArr[CANTIDAD_SENTENCIAS-1].ranking;
    }else{
        var valor_minimo = auxArr[auxArr.length-1].ranking;
    }
    var dimension_intervalo = (valor_maximo - valor_minimo)/CANTIDAD_INTERVALOS;*/
    var index=-1;
    
            
    if(auxArr.length>10){
        var values_clustering=[];
        
        for(var i = 0; i < CANTIDAD_SENTENCIAS; i++){
            values_clustering[i]=[auxArr[i].ranking, 0];
        }
      //console.log(values_clustering);
        var limites_escala_relevancia = obtener_escala_relevancia(values_clustering);
        //console.log(limites_escala_relevancia);
      
        

        for(var i = 0; i < CANTIDAD_SENTENCIAS; i++){
            arrayFormateado[arrayFormateado.length]= auxArr[i];
            if(typeof arrayFormateado[arrayFormateado.length-1] == 'object'){
                arrayFormateado[arrayFormateado.length-1]['posicion'] = i+1;
            }

            
            for(var s=0; s < CANTIDAD_INTERVALOS; s++){
                if(arrayFormateado[arrayFormateado.length-1]['ranking']>=(limites_escala_relevancia[s]['min']) && arrayFormateado[arrayFormateado.length-1]['ranking']<=(limites_escala_relevancia[s]['max'] )){
                    index = s;
                }
            }
            if(index==-1){
                index=4;
            }
            
            arrayFormateado[arrayFormateado.length-1]['relevancia'] = valores_discretos[index];  


        }
    }else{
        for(var i = 0; i < auxArr.length; i++){
            arrayFormateado[arrayFormateado.length]= auxArr[i];
            if(typeof arrayFormateado[arrayFormateado.length-1] == 'object'){
                arrayFormateado[arrayFormateado.length-1]['posicion'] = i+1;
            }

            /*for(var s=0; s < 5; s++){
                if(arrayFormateado[arrayFormateado.length-1]['ranking']>=(dimension_intervalo * s) && arrayFormateado[arrayFormateado.length-1]['ranking']<=(dimension_intervalo * (s+1)) ){
                    index = s;
                }
            }*/
            if(index==-1){
                index=4;
            }
            
            arrayFormateado[arrayFormateado.length-1]['relevancia'] = valores_discretos[index];  


        }
    }
    return arrayFormateado;
}

function obtener_escala_relevancia(values_clustering){
    var tree = clusterfck.hcluster(values_clustering, "euclidean", "average");
    var escala_relevancia = tree.clusters(CANTIDAD_INTERVALOS);
    var array_intervalos_relevancia=[];
    for(var i = 0; i<escala_relevancia.length;i++){
       var intervalos_relevancia = new Object();
       intervalos_relevancia['min']=obtener_valor_menor(escala_relevancia[i]);
       intervalos_relevancia['max']=obtener_valor_mayor(escala_relevancia[i]);
       array_intervalos_relevancia[i]=intervalos_relevancia;
    }
    console.log(array_intervalos_relevancia);
    array_intervalos_relevancia.sort((a, b)=> a.max - b.max);
    console.log('**************')
    console.log(array_intervalos_relevancia);
    return(array_intervalos_relevancia);
 }
 
 function obtener_valor_menor(vector){
 
    vector=obtener_nuevo_vector(vector);
    val=vector[0];
    for(var i=0; i < vector.length;i++){
       if(vector[i]<val){
          val=vector[i];
       }
    }
 
    return val;
 }
 function obtener_valor_mayor(vector){
 
    vector=obtener_nuevo_vector(vector);
    val=vector[0];
    for(var i=0; i < vector.length;i++){
       if(vector[i]>val){
          val=vector[i];
       }
    }
 
    return val;
 }
 
 function obtener_nuevo_vector(vector){
    var nuevo_vector=[];
    for(var i=0; i<vector.length;i++){
       nuevo_vector[i]=vector[i][0];
    }
 
    return nuevo_vector;
 }

 clasificacion_sentencias.discretizar_relevancia_test_sin_clustering = (auxArr, arrayFormateado, cantidad_test) => {
    CANTIDAD_SENTENCIAS = cantidad_test;
    var valor_maximo = auxArr[0].ranking;
    if(auxArr.length>10){
        var valor_minimo = auxArr[CANTIDAD_SENTENCIAS-1].ranking;
    }else{
        var valor_minimo = auxArr[auxArr.length-1].ranking;
    }
    var dimension_intervalo = (valor_maximo - valor_minimo)/CANTIDAD_INTERVALOS;
    var index=-1;
            
    if(auxArr.length>10){
        for(var i = 0; i < CANTIDAD_SENTENCIAS; i++){
            arrayFormateado[arrayFormateado.length]= auxArr[i];
            if(typeof arrayFormateado[arrayFormateado.length-1] == 'object'){
                arrayFormateado[arrayFormateado.length-1]['posicion'] = i+1;
            }

            for(var s=0; s < 5; s++){
                if(arrayFormateado[arrayFormateado.length-1]['ranking']>=(dimension_intervalo * s) && arrayFormateado[arrayFormateado.length-1]['ranking']<=(dimension_intervalo * (s+1)) ){
                    index = s;
                }
            }
            if(index==-1){
                index=4;
            }
            
            arrayFormateado[arrayFormateado.length-1]['relevancia'] = valores_discretos[index];  


        }
    }else{
        for(var i = 0; i < auxArr.length; i++){
            arrayFormateado[arrayFormateado.length]= auxArr[i];
            if(typeof arrayFormateado[arrayFormateado.length-1] == 'object'){
                arrayFormateado[arrayFormateado.length-1]['posicion'] = i+1;
            }

            for(var s=0; s < 5; s++){
                if(arrayFormateado[arrayFormateado.length-1]['ranking']>=(dimension_intervalo * s) && arrayFormateado[arrayFormateado.length-1]['ranking']<=(dimension_intervalo * (s+1)) ){
                    index = s;
                }
            }
            if(index==-1){
                index=4;
            }
            
            arrayFormateado[arrayFormateado.length-1]['relevancia'] = valores_discretos[index];  


        }
    }
    return arrayFormateado;
}


module.exports = clasificacion_sentencias;