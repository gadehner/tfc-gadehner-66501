var calculo_distancias_semanticas = {};
/**
 * Otros modulos
 */
var n_termino_sparql = require('./shared_functions/normalizar_termino_sparql');
var replace_all = require('./shared_functions/replace_all_terms'); 
var consulta_sparql = require('./shared_functions/consulta_sparql');
var consulta_mysql = require('./shared_functions/consulta_mysql');

const SpearmanRHO = require('spearman-rho');


var headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
    'Content-Type' : 'ext/html; charset=UTF-8'
}
const cheerio = require('cheerio'); //para scrapping
var request = require('request');

const natural = require('natural');
const shared = require('./shared-variables');
const ponderacion_maxima = shared.PONDERACION_MAXIMA;
const ponderacion_minima = shared.PONDERACION_MINIMA;

calculo_distancias_semanticas.obtener_distancias_semanticas_test = async (conjuntoBusquedaSentencias, parametro_de_busqueda, method, numero_iteraciones) => {

    auxPropiedades = []
    for(var h = 0; h < conjuntoBusquedaSentencias.length; h++){
        var objeto = new Object();
        objeto.term = conjuntoBusquedaSentencias[h];
        
        /** PRIMERO SE DEBE BUSCAR EN SPARQL Y LAS PONDERACIONES CON LAS RELACIONADAS
        * LUEGO SE BUSCA EN LO OTRO Y SI NO EXISTE RELACION, AHI SI SE BUSCA
        * LA NGD
        * 
        */
        var bd_sparql = '';
        
        if(method==2 || method==3){
            if(!(numero_iteraciones == 'undefined') && numero_iteraciones!==0){
                bd_sparql = 'bd_ngd_test1';
            }else{
                bd_sparql = 'bd_ngd';
            }
        }else{
            if(method==4 || method==5){
                if(!(numero_iteraciones == 'undefined') && numero_iteraciones!==0){
                    bd_sparql = 'bd_djn_test1';
                }else{
                    bd_sparql = 'bd_djn';
                }
            }
        }
       
        
        var termino_ = n_termino_sparql.normalizar_termino_sparql(objeto.term);
        var id_termino_= (await consulta_sparql.consultaSparql('select ?s ?p ?o where {:'+termino_+' :id ?o.}',bd_sparql)).resultado[0];
        if (typeof id_termino_ === "undefined"){
            id_termino_=0;
        }else{
            id_termino_=id_termino_.o.value;
            id_termino_=parseInt(id_termino_, 10);
        }
        //console.log(id_termino_);
        
        var parametro_ = n_termino_sparql.normalizar_termino_sparql(parametro_de_busqueda);
        var id_parametro_= (await consulta_sparql.consultaSparql('select ?s ?p ?o where {:'+parametro_+' :id ?o.}', bd_sparql)).resultado[0];
        if (typeof id_parametro_ === "undefined"){
            id_parametro_=0;
        }else{
            id_parametro_=id_parametro_.o.value;
            id_parametro_=parseInt(id_parametro_,10);
        }
        objeto.relacion = parametro_de_busqueda;

        //var resultado = (await consultaSparql('select ?s ?p ?o where {:'+termino_+' :'+parametro_+' ?o.}')).resultado; 
        
        var resultado = (await consulta_sparql.consultaSparql('select ?s ?p ?o where {:id_'+id_termino_.toString()+' :id_'+id_parametro_.toString()+' ?o.}', bd_sparql)).resultado; 
        //console.log('resultado');
        //console.log(resultado);



        if(resultado.length == 0){//no existe la relacion entre el termino de consulta y el obtenido
                                //por ello se busca la distancia semantica
            var ngd = 0;
            if(method==2 || method==3){
                ngd = ponderacion_maxima - (await distanciaNGD(conjuntoBusquedaSentencias[h], parametro_de_busqueda)).ngd;
            }else{
                if(method==4 || method==5){
                    ngd = (await distanciaDJN(conjuntoBusquedaSentencias[h], parametro_de_busqueda)).ngd;
                }
            }
            //con ngd debo hallar la ponderacion que debe ser igual al umbral maximo
            //menos ngd?
            objeto.ponderacion_resultante = /*ponderacion_maxima -*/ ngd;
            if(objeto.ponderacion_resultante < 0){
                objeto.ponderacion_resultante = ponderacion_minima;
            }
        }else{//existe la relacion y se obtiene la distancia semantica
            var valor_ponderacion = resultado.o;
            //valor_ponderacion = valor_ponderacion.replace('http://api.stardog.com/', '');
            //valor_ponderacion = parseFloat(valor_ponderacion);                                        
            objeto.ponderacion_resultante = valor_ponderacion;
        }
        //objeto.ngdTerm = objeto.ngdTerm.ngd;
        auxPropiedades[auxPropiedades.length] = objeto;
        
        
    }

    //console.log(auxPropiedades);

    //este no necesita REGEX, pq va tal cual el termino, en principio al menos
    var parametro_= n_termino_sparql.normalizar_termino_sparql(parametro_de_busqueda);
    var resultado3 = (await consulta_sparql.consultaSparql('select ?s ?p ?o where {:termino ?p "'+parametro_+'".}',bd_sparql)).resultado; 
    var id_parametro;
    if(resultado3.length == 0){//el parametro de busqueda no esta almacenado
         
        id_parametro = (await consulta_sparql.consultaSparql('SELECT ?nid'
                                                    +' WHERE { ?s :id ?nid }'
                                                    +' ORDER BY DESC(?nid) LIMIT 1', bd_sparql)).resultado[0];
        //console.log(id_parametro)
        if(typeof id_parametro === "undefined"){
            id_parametro=0;
        }else{
            id_parametro=id_parametro.nid.value;
            id_parametro = parseInt(id_parametro, 10);
        }
        id_parametro = id_parametro+1;
        (await consulta_sparql.consultaSparql('PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> '
                            +' INSERT DATA {'
                            +' :termino rdf:type "'+parametro_+'"^^xsd:string .'
                            +' :'+parametro_+' :id "'+id_parametro+'"^^xsd:integer .'
                            +'}',bd_sparql));
    }else{
        id_parametro = (await consulta_sparql.consultaSparql('select ?s ?p ?o where {:'+parametro_+' :id ?o.}',bd_sparql)).resultado[0].o.value;
    }
    for(var j = 0; j < auxPropiedades.length; j++){
        var termino_ = n_termino_sparql.normalizar_termino_sparql(auxPropiedades[j].term);

        var parametro_= n_termino_sparql.normalizar_termino_sparql(parametro_de_busqueda);
        //aca se requiere almacenar el parametro de busqueda para darle un id si es q no tiene..
        

        if(parametro_de_busqueda != auxPropiedades[j].term){// si no es el parametro de busqueda entonces se 
                                                //realiza insert de los terminos y relaciones
            
                                                //ACA IRIA EL REGEX
            var resultado = (await consulta_sparql.consultaSparql('select ?s ?p ?o where {?s rdf:type "'+termino_+'".}',bd_sparql)).resultado.length; 
            if(resultado == 0){ //si no esta almacenado el termino
                var nuevo_id = (await consulta_sparql.consultaSparql('SELECT ?nid'
                                                        +' WHERE { ?s :id ?nid }'
                                                        +' ORDER BY DESC(?nid) LIMIT 1',bd_sparql)).resultado[0].nid;
                
                //console.log('*-*-*-*-###*-$*-$*-$*$-');
                //console.log(nuevo_id);
                if(typeof nuevo_id === "undefined"){
                    nuevo_id=0;
                }else{
                    nuevo_id=nuevo_id.value;
                    nuevo_id = parseInt(nuevo_id, 10);
                }
                nuevo_id = nuevo_id+1;
                (await consulta_sparql.consultaSparql('PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> '
                    +'INSERT DATA {'
                    +' :termino rdf:type "'+termino_+'"^^xsd:string .'
                    +' :'+termino_+' :id "'+nuevo_id+'"^^xsd:integer .'
                    //+' :'+termino_+' :termino_relacionado :'+parametro_+'.'
                    +' :id_'+nuevo_id.toString()+' :id_'+id_parametro.toString()+' "'+auxPropiedades[j].ponderacion_resultante+'"^^xsd:float .'
                    +' }', bd_sparql));
            }else{ //el termino esta almacenado pero se verifica si la relacion esta almacenada
                    //+' :'+termino_+' :termino_relacionado :'+parametro_+'.'
                
                var id_termino = (await consulta_sparql.consultaSparql('select ?s ?p ?o where {:'+termino_+' :id ?o.}',bd_sparql)).resultado[0].o.value;
                
                var resultado2 = (await consulta_sparql.consultaSparql('select ?s ?p ?o where {:id_'+id_termino.toString()+' :id_'+id_parametro.toString()+ ' ?o.}',bd_sparql)).resultado.length; 
                if(resultado2 == 0){//si existe pero no esta relacionado
                    (await consulta_sparql.consultaSparql('INSERT DATA {'
                    +' :id_'+id_termino.toString()+' :id_'+id_parametro.toString()+ ' "'+auxPropiedades[j].ponderacion_resultante+'"^^xsd:float .'
                    //+' :'+termino_+' :'+parametro_+' :'+auxPropiedades[j].ponderacion_resultante
                    +' }',bd_sparql));
                } 
                if(resultado2 > 0){//si existe y esta relacionado, obtengo la ponderacion guardada...
                    var valor_ponderacion = (await consulta_sparql.consultaSparql('select ?s ?p ?o where {:id_'+id_termino+' :id_'+id_parametro+' ?o.}',bd_sparql)).resultado[0].o.value;
                    //valor_ponderacion = valor_ponderacion.replace('http://api.stardog.com/', '');
                    //valor_ponderacion = parseFloat(valor_ponderacion);
                    auxPropiedades[j].ponderacion_resultante = valor_ponderacion;
                    //console.log('valor ponderacion: ');
                    //console.log(valor_ponderacion);
                    //http://api.stardog.com/0.2890430522037447
                    

                }
            }
        }
    
    }
    return auxPropiedades;
}


async function distanciaDJN(termino1, termino2){
    ///var tokens = parametro_de_busqueda.split(' ');
    return new Promise( async function(resolve, reject){

    
        var valor = [0,0,0];
        valor[0] = await getCantidadResultadosLocales(natural.PorterStemmerEs.stem(termino1)) + 1;
        valor[1] = await getCantidadResultadosLocales(natural.PorterStemmerEs.stem(termino2)) + 1;
        valor[2] = await getCantidadResultadosConjunto(natural.PorterStemmerEs.stem(termino1), natural.PorterStemmerEs.stem(termino2)) + 1;
       
//console.log('valor1: '+valor[0] + '  valor2: '+valor[1]+ '  ambos: ' + valor[2]);

        var maximo = Math.max(Math.log2(valor[0]), Math.log2(valor[1]));
        var minimo = Math.min(Math.log2(valor[0]), Math.log2(valor[1]));
        var logN = Math.log2(parseFloat(3393000));//documentos legales indexados *1000
        var numerador = maximo - (Math.log2(valor[2]));
        var denominador =  logN - minimo;
        var normalizedLocalDistance = numerador/denominador;
        //console.log(valor);
        //console.log('La distancia semantica entre los terminos '+termino1 + ' y '+termino2+' es: '+normalizedLocalDistance);
        /**
         * Coeficiente de Pearson
         */
        x_media=0;
        x_varianza=0;/*cuadrado*/
        x_desviacion=0;

        y_media=0;
        y_varianza=0;
        y_desviacion=0;
        xy_covarianza=0;
        x_por_y_suma = 0;
        n_=0;
        coeficiente_correlacion_pearson = 0;
        coeficiente_spearman = 0;
        var varia1 = [];
        var varia2= [];
        if(termino1 !== termino2 && valor[2] > 1){
            
            var interseccion_sentencias = await getSentenciasInterseccion(natural.PorterStemmerEs.stem(termino1), natural.PorterStemmerEs.stem(termino2));
            
            for(var i = 0; i < interseccion_sentencias.length; i++){
                //console.log(interseccion_sentencias[i]);
                //console.log(interseccion_sentencias[i].texto);
                var x=0;
                var y=0;
                x = ((interseccion_sentencias[i].texto.match(new RegExp(natural.PorterStemmerEs.stem(termino1), 'gi')) || []).length);
                y = ((interseccion_sentencias[i].texto.match(new RegExp(natural.PorterStemmerEs.stem(termino2), 'gi')) || []).length); 
                x_media = x_media + x;
                y_media = y_media + y;
                x_varianza = x_varianza + Math.pow(x, 2);
                y_varianza = y_varianza + Math.pow(y, 2);
                xy_covarianza = xy_covarianza +( x * y );
                n_=i;
                varia1[varia1.length] = x;
                varia2[varia2.length] = y;

    
            }

            
            n_=n_+1;
            x_media = x_media/n_;
            y_media = y_media/n_;
            x_varianza = (x_varianza/n_) - (Math.pow(x_media, 2));
            y_varianza = (y_varianza/n_) - (Math.pow(y_media, 2));
            if(x_varianza>0 && y_varianza>0){
                x_desviacion = Math.pow(x_varianza, (1/2));
                y_desviacion = Math.pow(y_varianza, (1/2));
                xy_covarianza = (xy_covarianza/n_) - (x_media * y_media);
                coeficiente_correlacion_pearson = (xy_covarianza/(x_desviacion * y_desviacion));
                //console.log(x_media);
                //console.log(y_media);
                //console.log('coeficiente pearson entre  '+ termino1 + '  y  ' +termino2+' - '+n_+' '+x_media+' '+y_media+' '+x_varianza+' '+y_varianza+' '+x_desviacion+' '+y_desviacion+' '+xy_covarianza);
                //console.log(coeficiente_correlacion_pearson);
            }else{
                //console.log('estrecha relaci[on daria NaN pq siempre da igual');
                coeficiente_correlacion_pearson=1;
            }
            coeficiente_spearman = (await spearman(varia1,varia2)).coef_spearman;
                        
        }

        //console.log("coef pearson entre "+termino1+" y "+termino2);
        //console.log(coeficiente_correlacion_pearson);
        
        if(coeficiente_correlacion_pearson<0){
            coeficiente_correlacion_pearson = coeficiente_correlacion_pearson * (-1);
            

        }
        if(coeficiente_spearman<0){
            coeficiente_spearman = coeficiente_spearman * (-1);
            

        }

        var temporal = 1 - normalizedLocalDistance;
        var temp2= temporal;
        normalizedLocalDistance = temporal;
        if(coeficiente_correlacion_pearson !== 0 && coeficiente_spearman!==0){
            temporal = Math.pow((normalizedLocalDistance * ((coeficiente_correlacion_pearson+coeficiente_spearman)/2)), (1/2));
            normalizedLocalDistance = temporal;
        }


        if(isNaN(normalizedLocalDistance)){
            normalizedLocalDistance = temp2;
        }

        console.log('normalized: ' + temp2 +' pearson: '+coeficiente_correlacion_pearson+' spearman: '+coeficiente_spearman+" total: "+normalizedLocalDistance);
        resolve({ngd: normalizedLocalDistance});
    });
    //return normalizedGoogleDistance;
}
function spearman(variable1,variable2){
    var x = variable1;
    var y = variable2;
    const spearmanRHO = new SpearmanRHO(x, y);
    return new Promise( function(resolve, reject){
        spearmanRHO.calc().then(value => {
            resolve({coef_spearman: value});
        })
        .catch(err => console.error(err));  
    });
}
async function getSentenciasInterseccion(termino1, termino2){
    var query = "SELECT texto_resolucion as texto FROM `resoluciones` " 
    +" WHERE (texto_resolucion LIKE '%"+termino1+"%' OR caratula LIKE '%"+termino1+"%') "
    +" AND (texto_resolucion LIKE '%"+termino2+"%' OR caratula LIKE '%"+termino2+"%') ";
    //console.log((await consulta_mysql.consultaMysql(query)));
    return (await consulta_mysql.consultaMysql(query)).documentos;
}


async function getCantidadResultadosLocales(parametro){
    var query = "SELECT count(*) as cont FROM `resoluciones` WHERE texto_resolucion LIKE '%"+parametro+"%' OR caratula LIKE '%"+parametro+"%'";
    
    //console.log((await consultaMysql(query, con)).documentos);

    return (await consulta_mysql.consultaMysql(query)).documentos[0].cont;
}
async function getCantidadResultadosConjunto(termino1, termino2){
    var query = "SELECT count(*) as cont FROM `resoluciones` " 
                +" WHERE (texto_resolucion LIKE '%"+termino1+"%' OR caratula LIKE '%"+termino1+"%') "
                +" AND (texto_resolucion LIKE '%"+termino2+"%' OR caratula LIKE '%"+termino2+"%') ";
    return (await consulta_mysql.consultaMysql(query)).documentos[0].cont;
}

function getCantidadResultadosGoogle(parametro){
    //var cantidadResultados=0;
    var options = {
        url: 'https://www.google.com.ar/search?ei=rHP4XIyTJeKc5OUPgaqIaA&q='+parametro,
        method: 'GET',
        headers: headers
        //qs: {'key1': 'xxx', 'key2': 'yyy'}
    }
    //console.log('https://www.google.com.ar/search?ei=rHP4XIyTJeKc5OUPgaqIaA&q='+parametro);
    return new Promise(function (resolve, reject){
        var resultado='asd';
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                // Print out the response body
                // console.log(body)
                const $ = cheerio.load(body);
                //console.log(arra);
                //console.log($('#resultStats').html());asd abajo asd
                resultado = $('#resultStats').html();
                if(resultado!=null){
                    resultado = resultado.split(' ');
                    resultado = resultado[2];
                    /*resultado = resultado.replace(',','');
                    resultado = resultado.replace(',','');
                    resultado = resultado.replace(',','');
                    resultado = resultado.replace(',','');*/
                    resultado = replace_all.replaceAll(resultado, ',', '');
                    resultado = parseFloat(resultado);
                }else{
                    resultado =10000;
                }
            }
            resolve({cantidadResultadosGoogle: resultado});
        })

    });

               // resolve({cantidadResultadosGoogle: cantidadResultados});
            
}

async function distanciaNGD(termino1, termino2){
    ///var tokens = parametro_de_busqueda.split(' ');
    return new Promise( async function(resolve, reject){

    
        var valor = [0,0,0];
      //  await delay(3000);
        valor[0] = await getCantidadResultadosGoogle(termino1);
      //  await delay(3000);
        valor[1] = await getCantidadResultadosGoogle(termino2);
      //  await delay(3000);
        valor[2] = await getCantidadResultadosGoogle(termino1+ ' '+termino2);

    /* valor[0] = await getCantidadResultadosBing(tokens[0]);
        valor[1] = await getCantidadResultadosBing(tokens[1]);
        valor[2] = await getCantidadResultadosBing(parametro_de_busqueda);*/
        var maximo = Math.max(Math.log2(valor[0].cantidadResultadosGoogle), Math.log2(valor[1].cantidadResultadosGoogle));
        var minimo = Math.min(Math.log2(valor[0].cantidadResultadosGoogle), Math.log2(valor[1].cantidadResultadosGoogle));
        var logN = Math.log2(parseFloat(25270000000000));//documentos indexados aprox de google
        var numerador = maximo - (Math.log2(valor[2].cantidadResultadosGoogle));
        var denominador =  logN - minimo;
        var normalizedGoogleDistance = numerador/denominador;
        //console.log(valor);
        if(isNaN(normalizedGoogleDistance)){
            normalizedGoogleDistance = 0.95;
        }
        console.log('DS entre '+termino1 + ' y '+termino2+' es: '+normalizedGoogleDistance);
        resolve({ngd: normalizedGoogleDistance});
    });
    //return normalizedGoogleDistance;
}

module.exports = calculo_distancias_semanticas;