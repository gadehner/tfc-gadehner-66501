'use strict'
var clusterfck = require("tayden-clusterfck");
const CANTIDAD_INTERVALOS = 5;

function obtener_escala_relevancia(values_clustering){
   var tree = clusterfck.hcluster(values_clustering, "euclidean", "average");
   var escala_relevancia = tree.clusters(CANTIDAD_INTERVALOS);
   var array_intervalos_relevancia=[];
   for(var i = 0; i<escala_relevancia.length;i++){
      var intervalos_relevancia = new Object();
      intervalos_relevancia['min']=obtener_valor_menor(escala_relevancia[i]);
      intervalos_relevancia['max']=obtener_valor_mayor(escala_relevancia[i]);
      array_intervalos_relevancia[i]=intervalos_relevancia;
   }
   console.log(array_intervalos_relevancia);
   array_intervalos_relevancia.sort((a, b)=> b.max - a.max);
   console.log('**************')
   console.log(array_intervalos_relevancia);
   return(array_intervalos_relevancia);
}
////

/*
var colors = [
   [74.7415827526267,0],
[59.19346308300451,0],
[56.69861243303669,0],
[40.922115321000014,0],
[35.29995916087557,0],
[32.018957481,0],
[31.200925163502248,0],
[21.36663953,0],
[19.55835798,0],
[18.582603950875562,0],
[17.777787061,0],
[15.98327773,0],
[15.532518940000001,0],
[14.506573220875563,0],
[14.049030280000002,0],
[14.049030280000002,0],
[13.701920370000003,0],
[13.429018809999999,0],
[12.80960936,0],
[12.37047044,0],
[11.885525740875563,0],
[11.07921411,0],
[10.85349904,0],
[9.846197839999999,0],
[9.619519069999999,0],
[8.42874092,0],
[8.079480639999998,0],
[7.677311899999999,0],
[7.1183457400000005,0],
[6.589518259999999,0],
[4.57378956,0],
[4.060978900875563,0],
[3.2868415008755627,0],
[2.38024975,0],
[2.1849602800000003,0],
[1.8689041700000002,0],
[1.58327719,0],
[1.0321832,0],
[1.0321832,0],
[0.7741374,0],
[0.7741374,0],
[0.5389267600000001,0],
[0.5160916,0],
[0.3080458,0],
[0.28088096,0],
[0.2580458,0],
[0.2580458,0],
[0.2580458,0],
[0,0],
[0,0]
];
*/

///////////functions
function obtener_valor_menor(vector){

   vector=obtener_nuevo_vector(vector);
   val=vector[0];
   for(var i=0; i < vector.length;i++){
      if(vector[i]<val){
         val=vector[i];
      }
   }

   return val;
}
function obtener_valor_mayor(vector){

   vector=obtener_nuevo_vector(vector);
   val=vector[0];
   for(var i=0; i < vector.length;i++){
      if(vector[i]>val){
         val=vector[i];
      }
   }

   return val;
}

function obtener_nuevo_vector(vector){
   var nuevo_vector=[];
   for(var i=0; i<vector.length;i++){
      nuevo_vector[i]=vector[i][0];
   }

   return nuevo_vector;
}
module.exports = obtener_escala_relevancia;