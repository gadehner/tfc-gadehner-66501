var normalizar_propiedad = {};

normalizar_propiedad.normalizar_propiedad = (termino_propiedad) => {
    var bus = termino_propiedad.replace(/ /g, '_');
    bus = bus.replace(/á/g, 'a');
    bus = bus.replace(/é/g, 'e');
    bus = bus.replace(/í/g, 'i');
    bus = bus.replace(/ó/g, 'o');
    bus = bus.replace(/ú/g, 'u');
    bus = bus.replace(/ñ/g, 'n');
    bus = bus.replace(/\(/g, '');
    bus = bus.replace(/\)/g, '');
    bus = bus.replace(/,/g, '');
    return bus;
}



module.exports = normalizar_propiedad;