var norm_termino_sparql = {};
var replaceAll = require('./replace_all_terms');

norm_termino_sparql.normalizar_termino_sparql = (termino_objeto) => {
    var termino_ = termino_objeto.replace(/ /g, '_');
    termino_ = termino_.replace('(','');
    termino_ = termino_.replace(')','');
    termino_ = termino_.replace('°','');
    termino_ = replaceAll.replaceAll(termino_,'.', '');
    termino_ = replaceAll.replaceAll(termino_,',', '');
    return termino_;
}




module.exports = norm_termino_sparql;