var replace_all = {};

replace_all.replaceAll = ( text, busca, reemplaza ) => {
    while (text.toString().indexOf(busca) != -1)
        text = text.toString().replace(busca,reemplaza);
    return text;
}

module.exports = replace_all;