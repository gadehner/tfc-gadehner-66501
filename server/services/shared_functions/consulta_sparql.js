var consulta_sparql = {};
const { Connection, query } = require('stardog');
const conn = new Connection({
    username: 'admin',
    password: 'admin',
    endpoint: 'http://localhost:5820',
});

consulta_sparql.consultaSparql = (query_, BD) => {
    return new Promise(function (resolve, reject){
        query.execute(conn, BD, query_, 'application/sparql-results+json', {
            offset: 0
        }).then(function ({ body }) {
            //console.log(query_);
            //console.log(body);
            var retorno=null;
            if(body!=null){
                retorno = body.results.bindings;
            }
     
        resolve({resultado: retorno});
        });
    });
}

module.exports = consulta_sparql;