var expansion_consulta = {};
var n_propiedad = require('./shared_functions/normalizar_propiedad');
const getJSON = require('get-json');
var headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
    'Content-Type' : 'ext/html; charset=UTF-8'
}
const cheerio = require('cheerio'); //para scrapping
var request = require('request');
const utf8=require('utf8');
const axios = require('axios'); //para scraping
var parser = require('xml2json');
const natural = require('natural');

expansion_consulta.expandir_consulta = async (parametro_de_busqueda) => {
    var objetoApis = new Object();
    var palabrasRelacionadas = [];
    
    palabrasRelacionadas = await obtenerPalabrasConceptNet(parametro_de_busqueda);
    var C = [];
    C = eliminarPalabrasRepetidas(palabrasRelacionadas.relacionadas);
    for(var i=0; i < C.length; i++){
        var bus = n_propiedad.normalizar_propiedad(C[i]);
        objetoApis[bus]='ConceptNet';
    }
    
    C = await obtenerSinonimosWordreference(parametro_de_busqueda, C);
    C = eliminarPalabrasRepetidas(C.words);
    for(var i=0; i < C.length; i++){
        var bus = n_propiedad.normalizar_propiedad(C[i]); 
        if(objetoApis.hasOwnProperty(bus)){
            objetoApis[bus]=objetoApis[bus] + ', WordReference';
        }else{
            objetoApis[bus]='WordReference';
        }
    }
        
        
/*
*2
*/
    var conjuntoBusquedaSentencias = [];
    C[C.length] = parametro_de_busqueda;
    C =  eliminarPalabrasRepetidas(C);
    //aca faltaria eliminar palabra repetida por si detecta un sinonimo como la 
    //misma palabra
    //console.log("aca va!!!!");
    //console.log(C);
    
    for(var i = 0; i < C.length; i++){
        //id del para la palabra en el saij
        var aux = null
        //console.log('!!!!!!!!!!!!!!!!!!!!palabra'+ '  '+ C[i]);
        aux = await obtenerIdOntologiaSaij(C[i]);
        /**
         * Los RESULTADOS DEBO PROCESARLOS PARA VER A QUÉ PALABRA 
         * CORRESPONDEN Y EN FUNCION DE ESO REALIZAR LA BUSQUEDA..
         * PUEDE QUE SE REQUIERA OTRO FOR SI SE ELIGE MÁS DE UN TÉRMINO
         */
        /**si el index es distinto a -1 entonces la busqueda coincide exactamente 
             * y objetoApis[C[i]]=objetoApis[C[i]]+', SAIJ';
            */
        var index = -1;
        index = aux.indice;
        if(index !== -1){
            var bus1 = n_propiedad.normalizar_propiedad(C[i]);
            if(objetoApis.hasOwnProperty(bus1)){
                if((objetoApis[bus1].match(new RegExp('SAIJ', 'gi')) || []).length==0){
                    objetoApis[bus1]=objetoApis[bus1] + ', SAIJ';
                }
            }else{
                objetoApis[bus1]='SAIJ';
            }  
        }
        aux = aux.resultadoSaij;
        conjuntoBusquedaSentencias[conjuntoBusquedaSentencias.length]= C[i];
        //Con el id se busca los terminos
        if(aux != null){
            var terminosSaij = null;
            terminosSaij = await obtenerTerminosSaij(aux.term_id);
            //console.log(terminosSaij);
            for(var j=0; j < terminosSaij.resultadoSaij.length; j++){
                conjuntoBusquedaSentencias[conjuntoBusquedaSentencias.length] = terminosSaij.resultadoSaij[j].termName;
                //console.log(terminosSaij.resultadoSaij[j].termName);
                
                var bus = n_propiedad.normalizar_propiedad(terminosSaij.resultadoSaij[j].termName);
                
                //console.log('posic arr   :'+C[i]);
                if(objetoApis.hasOwnProperty(bus)){
                    if((objetoApis[bus].match(new RegExp('SAIJ', 'gi')) || []).length==0){
                        objetoApis[bus]=objetoApis[bus] + ', SAIJ';
                    }
                }else{
                    objetoApis[bus]='SAIJ';
                }   
            }
            
            
        }  
    }
    console.log(objetoApis);        
    
    conjuntoBusquedaSentencias = eliminarPalabrasRepetidas(conjuntoBusquedaSentencias);
    console.log(conjuntoBusquedaSentencias);
    var json = {
        'cbs':conjuntoBusquedaSentencias,
        'oapis': objetoApis
    }

    return json;
    
}

function obtenerPalabrasConceptNet(termino){
    var array = [];
    return new Promise(function(resolve, reject){
        var url = 'http://api.conceptnet.io/c/es/'+termino+'?offset=0&limit=500';
        //url = utf8.encode(url);
        url = encodeURI(url);
        getJSON(url)
        .then(function(response) {            
            for(var i=0; i< response.edges.length; i++){
                //console.log(response.edges[i].rel);
                if(response.edges[i].start.language=='es'){
                    if(response.edges[i].start.label!='' && response.edges[i].rel.label != 'FormOf' ){
                        array[array.length]=response.edges[i].start.label;
                    }
                }
                if(response.edges[i].end.language=='es'){
                    if(response.edges[i].end.label!='' && response.edges[i].rel.label != 'FormOf' ){
                        array[array.length]=response.edges[i].end.label;
                    }
                }
            }
            
            resolve({relacionadas: array});
        }).catch(function(error) {
            console.log(error);
        });
    });
}

function eliminarPalabrasRepetidas(array){
    arraySinRepetidos = [];
    for(var i=0;i<array.length;i++){
        //array[i]=array[i].replace(/'/g,'');
        //array[i]=array[i].replace(/"/g,'');
        
        
        if(array[i] != 'undefined' && array[i] != ''){
            if(habilitarAniadir(array[i], arraySinRepetidos)){
                arraySinRepetidos[arraySinRepetidos.length] = array[i];
            }
        }
    } 
    return arraySinRepetidos;
}
function habilitarAniadir(valor, arraySinRepetidos){
    retorno = true;
    for(var i=0; i<arraySinRepetidos.length; i++){
        if(natural.PorterStemmerEs.stem(arraySinRepetidos[i].toLowerCase())==natural.PorterStemmerEs.stem(valor.toLowerCase())){
            retorno = false;
        }
    }
    return retorno;
}

function obtenerSinonimosWordreference(termino, C){
    var array = [];
    var options = {
        url: encodeURI('https://www.wordreference.com/sinonimos/'+termino),
        method: 'GET',
        headers: headers
        //qs: {'key1': 'xxx', 'key2': 'yyy'}
    }
    return new Promise(function (resolve, reject){
        var resultado='NaN';
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                // Print out the response body
                // console.log(body)
                const $ = cheerio.load(body);
                //console.log(body);
                //console.log($('#resultStats').html());asd abajo asd
                //resultado = $('#article ul').html();
                cantidadResultados= $('#article ul').html();
                    //console.log('http://www.wordreference.com/sinonimos/'+termino);
                    console.log(cantidadResultados);
                    if(cantidadResultados != null){
                        cantidadResultados=cantidadResultados.replace(/<li>/g, ' ');
                        cantidadResultados=cantidadResultados.replace(/<br>/g, '');
                        cantidadResultados=cantidadResultados.replace(/<\/li>/g, ' ');
                        cantidadResultados=cantidadResultados.replace(/<\/ul>/g, '<ul>');
                        
                        cantidadResultados=cantidadResultados.replace(/&#xE1;/g, 'á');
                        cantidadResultados=cantidadResultados.replace(/&#xE9;/g, 'é');
                        cantidadResultados=cantidadResultados.replace(/&#xED;/g, 'í');
                        cantidadResultados=cantidadResultados.replace(/&#xF3;/g, 'ó');
                        cantidadResultados=cantidadResultados.replace(/&#xFA;/g, 'ú');
                        cantidadResultados=cantidadResultados.replace(/&#xF1;/g, 'ñ');
                        cantidadResultados=cantidadResultados.replace(/ /g, '');
                        //console.log(cantidadResultados);
                        cantidadResultados= cantidadResultados.split('<ul>');
                        cantidadResultados= cantidadResultados[0]+','+cantidadResultados[2];
                    }else{
                        cantidadResultados = [];
                    }

                    if(cantidadResultados.length > 0){
                        palabrasRelacionadasWR = cantidadResultados.split(',');
                        for(var i = 0; i < palabrasRelacionadasWR.length; i++){
                            C[C.length] = palabrasRelacionadasWR[i];
                        }
                        
                    }

                }
                resolve({words: C});
           

    });
    
});
}


function obtenerIdOntologiaSaij(parametroBusqueda){
    return new Promise(function (resolve, reject){
        var url = 'http://vocabularios.saij.gob.ar/saij/services.php?task=search&arg='+parametroBusqueda;
        url = utf8.encode(url);
        axios.get(url)
            .then((response) => {
                if(response.status === 200) {
                    const xml = response.data;
                    var index = -1;
                    // xml to json
                    if(xml.indexOf('term') != -1){
                        var json1 = parser.toJson(xml);
                        var json2 = JSON.stringify(eval("(" + json1 + ")"));
                        var json = JSON.parse(json2);
                        //console.log(json.vocabularyservices.result);
                        //console.log(json.vocabularyservices.result.term[0].term_id);
                        if(json.vocabularyservices.hasOwnProperty('result')){
                            //json = json.vocabularyservices.result.term[0];
                            var jsons = json.vocabularyservices.result.term;
                            //console.log(jsons);
                           
        /**
         * Se busca si hay exactamente el termino buscado y sino se asigna el primero 
         * que este relacionado
         */
                            for(var i=0; i < jsons.length; i++){
                                
                                if(jsons[i].string==parametroBusqueda){
                                    index = i;
                                }
                                //console.log(jsons[i].string +' == '+ parametroBusqueda+'    index: '+index);
                            }
                            if(index == -1){
                                json = jsons[0];
                                if(!(jsons.hasOwnProperty('length'))){
                                    if(jsons.string==parametroBusqueda){
                                        index=1;
                                    }
                                }
                                
                            }else{/*Indice con busqueda exacta..  !=-1 distinto a -1
                                puedo mandar index para saber si fue o no exacta la coincidencia*/
                                json = jsons[index];
                                
                            }
                            
                        }else{
                            json = null;
                        }
                    }else{
                        json = null;
                        //console.log(parametroBusqueda);
                    }
                }
                /*var json = {
                    'cbs':conjuntoBusquedaSentencias,
                    'oapis': objetoApis
                }*/
                resolve({resultadoSaij: json, indice:index});
            }, (error) => console.log(err) );
    });
}

function obtenerTerminosSaij(id){
    return new Promise(function (resolve, reject){
        var url = 'http://vocabularios.saij.gob.ar/saij/xml.php?zthesTema='+id;
        url = utf8.encode(url);
        axios.get(url)
            .then((response) => {
                if(response.status === 200) {
                    const xml = response.data;                         
                    // xml to json
                    var json1 = parser.toJson(xml);
                    var json2 = JSON.stringify(eval("(" + json1 + ")"));
                    var json = JSON.parse(json2);
                    console.log("-----------");
                    //console.log(json.Zthes.term.relation);
                    json = json.Zthes.term.relation;
                    
                }
                resolve({resultadoSaij: json});
            }, (error) => console.log(err) );
    });
}






module.exports = expansion_consulta;