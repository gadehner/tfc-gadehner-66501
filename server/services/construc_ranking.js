var construc_ranking = {};
const natural = require('natural');
/**
 * coneccion con otros modulos
 */
var n_propiedad = require('./shared_functions/normalizar_propiedad');

construc_ranking.confeccionar_ranking = (rta, auxPropiedades, parametro_de_busqueda, objetoApis, method) => {
    var minimo=0;
    var maximo=0;
    var elevasumatoria=0;
    var arrayFormateado = [];
    var ids1 = [];
    var ids2 = [];
    for(var t = 0; t < rta.length; t++){
        for(var y = 0; y < rta[t].length; y++){
            arrayFormateado[arrayFormateado.length] = rta[t][y];  
            ids1[ids1.length] = rta[t][y].id1; 
            ids2[ids2.length] = rta[t][y].id2; 
        }
    }
    var ids=[];
    ids = eliminarPalabrasRepetidasDosIds(ids1, ids2);
    var auxArr = [];//variable que almacena los documentos, calificaciones y demas.

    for(var t = 0; t < arrayFormateado.length; t++){
        for(var y = 0; y < ids.length; y++){
            if(ids[y].id1 == arrayFormateado[t].id1 && ids[y].id2 == arrayFormateado[t].id2){
                auxArr[auxArr.length] = arrayFormateado[t];
                auxArr[auxArr.length-1]['trazabilidad']='undefined';
                auxArr[auxArr.length-1]['ranking'] = 0;
                auxArr[auxArr.length-1]['calificacion'] = 'undefined';
                auxArr[auxArr.length-1]['posicion'] = 'undefined';
                auxArr[auxArr.length-1]['parametro_de_busqueda'] = parametro_de_busqueda;
                auxArr[auxArr.length-1]['relevancia'] = 'undefined';
                ids.splice(y, 1);
            }  
        }
    }
    //auxPropiedades = auxPropiedades[0];
   // console.log('!!!!!!!!!!!!!!AUXPropiedades');
   // console.log(auxPropiedades);
    //console.log(auxPropiedades);
    //console.log(parametro_de_busqueda);
    console.log(parametro_de_busqueda);
    var CREDITOS = 0;
    var PUNTAJESAIJ = 100000;//
    //Mas abajo se reasigna el valor con el puntaje de la sentencia con mayor puntaje, donde
    //dicha sentencia no haya tenido ocurrencias del saij.
    var PUNTAJEDEMAS = 0;
    for(var t = 0; t < auxArr.length; t++){
        var seguimiento = [];
        for(var h=0; h< parametro_de_busqueda.length;h++){
            var seguimiento_temporal=[];
            //console.log('!!!!!!asdads');
            //console.log(parametro_de_busqueda);
         
            
            for(var y = 0; y < auxPropiedades[h].length; y++){//for que contemple ranking 1,2,3
                /**
                 * auxPropiedades posee termino, relacion y ponderacion resultante
                 * tiene dimensiones como cantidad de terminos haya ingresado el usuario
                 */


/**
 * con stemming
 * var count = (auxArr[t].texto.match(new RegExp(natural.PorterStemmerEs.stem(auxPropiedades[h][y].term), 'gi')) || []).length
    var count_titulo = (auxArr[t].title.match(new RegExp(natural.PorterStemmerEs.stem(auxPropiedades[h][y].term), 'gi')) || []).length
 */
                var cantidad_palabras = auxArr[t].texto.length;
                var texto_temporal = auxArr[t].texto;
                auxArr[t].texto=tratamiento_cadenas(auxArr[t].texto);
                var titulo_temporal = auxArr[t].title;
                auxArr[t].title=tratamiento_cadenas(auxArr[t].title);
                
                var count=0;
                var count_titulo=0;
                /**
     * a auxArr le puedo agregar una propiedad que diga si tiene terminos del saij o no.
     * ordenar primero los del saij y luego los otros
     * considerar que si aparece en el titulo el termino, esta en el top
     */
                var bus1 = n_propiedad.normalizar_propiedad(auxPropiedades[h][y].term); 
                var es_termino_saij = 0;
                if(objetoApis[h].hasOwnProperty(bus1)){
                    if((objetoApis[h][bus1].match(new RegExp('SAIJ', 'gi')) || []).length>0){
                        //console.log(objetoApis[h][bus1]);
                        //(objetoApis[bus1].match(new RegExp('SAIJ', 'gi')) || []).length==0
                        es_termino_saij = 1;
                    }
                }
    
                if(!auxArr[t].hasOwnProperty(['titulo_saij'])){
                    auxArr[t]['titulo_saij']=0;
                    auxArr[t]['cuerpo_saij']=0;
                    auxArr[t]['consulta_busqueda']=0;
                    

                    auxArr[t]['svalorti']=0.0;
                    auxArr[t]['svalorcu']=0.0;
                    auxArr[t]['svalorco']=0.0;


                }
                count = ((auxArr[t].texto.match(new RegExp(natural.PorterStemmerEs.stem(auxPropiedades[h][y].term), 'gi')) || []).length);
                count_titulo = ((auxArr[t].title.match(new RegExp(natural.PorterStemmerEs.stem(auxPropiedades[h][y].term), 'gi')) || []).length);
                
                if(es_termino_saij && count_titulo > 0){
                    auxArr[t]['titulo_saij']+=1;
                   // console.log('llego titulo');
                   auxArr[t]['svalorti']+=parseFloat(auxPropiedades[h][y].ponderacion_resultante);
                }
                if(es_termino_saij && count > 0){
                    auxArr[t]['cuerpo_saij']+=1;
                    //console.log('llego cuerpo');
                    auxArr[t]['svalorcu']+=parseFloat(auxPropiedades[h][y].ponderacion_resultante);
                }
                if(parametro_de_busqueda[h]==auxPropiedades[h][y].term &&(count_titulo > 0 || count > 0)){
                    auxArr[t]['consulta_busqueda']+=1;

                    auxArr[t]['svalorco']+=parseFloat(auxPropiedades[h][y].ponderacion_resultante);
                }
                
                
                
                auxArr[t].texto=texto_temporal; 
                auxArr[t].title=titulo_temporal; 
                var count_temporal=count;
                if(count > 0 && (method==3 || method==5)){
                    count = count/cantidad_palabras;
                }
                    
                //}

                auxArr[t].ranking = auxArr[t].ranking + (count * auxPropiedades[h][y].ponderacion_resultante) + (count_titulo * auxPropiedades[h][y].ponderacion_resultante);
                
                var objeto_seguimiento = new Object();
                count=count_temporal;
                objeto_seguimiento.termino = auxPropiedades[h][y].term;
                objeto_seguimiento.ocurrencia = (count/*cantidad_palabras*/) + count_titulo;
                objeto_seguimiento.ponderacion_resultante = auxPropiedades[h][y].ponderacion_resultante;
                objeto_seguimiento.relacion = auxPropiedades[h][y].relacion;
                
                var bus = n_propiedad.normalizar_propiedad(objeto_seguimiento.termino); 
            
                var valor_api;
                if(objetoApis[h].hasOwnProperty(bus)){
                    valor_api=objetoApis[h][bus];
                }else{
                    valor_api='N/A';
                }
                
                objeto_seguimiento.apis = valor_api;
                if(objeto_seguimiento.ocurrencia > 0){
                    seguimiento_temporal[seguimiento_temporal.length] = objeto_seguimiento;
                }
            }
            seguimiento[seguimiento.length] = seguimiento_temporal;
        }
        auxArr[t].trazabilidad = seguimiento;
        if(!(auxArr[t]['titulo_saij'] || auxArr[t]['cuerpo_saij'] || auxArr[t]['consulta_busqueda'])){
            if(CREDITOS < auxArr[t].ranking){
                CREDITOS = auxArr[t].ranking;
            }
        }else{
            if(PUNTAJESAIJ > auxArr[t].ranking){
                PUNTAJESAIJ = auxArr[t].ranking;
            }
        }
    }
    console.log(CREDITOS);
    if(CREDITOS>PUNTAJESAIJ){
        CREDITOS=(CREDITOS-PUNTAJESAIJ);
    }else{
        CREDITOS=0;
    }
    //CREDITOS=0;
    console.log(CREDITOS);

    for(var t = 0; t < auxArr.length; t++){
        //console.log('llego');
        //console.log(auxArr[t]['titulo_saij_busqueda']);
        auxArr[t]['ranking_sin_credito'] = auxArr[t]['ranking'];
        var valor_semantico=1;
        if(auxArr[t]['titulo_saij']>0){
            //valor_semantico = auxArr[t]['svalorti']/auxArr[t]['titulo_saij'];

            auxArr[t]['ranking']=auxArr[t]['ranking']+(CREDITOS*valor_semantico);
            //console.log('sisititulo');
            
        }
        //console.log(auxArr[t]['cuerpo_saij_busqueda']);
        if(auxArr[t]['cuerpo_saij']>0){
            //valor_semantico = auxArr[t]['svalorcu']/auxArr[t]['cuerpo_saij'];

            auxArr[t]['ranking']=auxArr[t]['ranking']+(CREDITOS*valor_semantico);
            //console.log('sisicuerpo');
            
        }   
        if(auxArr[t]['consulta_busqueda']>0){
            //valor_semantico = auxArr[t]['svalorco']/auxArr[t]['consulta_busqueda'];
            
            auxArr[t]['ranking']=auxArr[t]['ranking']+(CREDITOS*valor_semantico);
        
        }        
    }

   //console.log(auxArr[0].trazabilidad);
    /**
     * auxArr tiene id1, id2, anio resolucion, title/caratula, texto/cuerpo
     * un array de trazabilidad, puntuacion ranking, calificacion, posicion, parametro de busqueda
     * y relevancia. 
     * Trazabilidad es un array de dos dimensiones. en la segunda tiene termino, ocurrencia, 
     * ponderacion resultante, relacion (la palabra con la que tiene relacion) y apis por las que
     * fue encontrada.
     *  */
    

    return auxArr;

}
function tratamiento_cadenas(texto){
    //para que los acentos no impidan contabilizar algunos terminos lematizados.
    texto = texto.toLowerCase();
    texto = texto.replace(/á/gi,"a");
    texto = texto.replace(/é/gi,"e");
    texto = texto.replace(/í/gi,"i");
    texto = texto.replace(/ó/gi,"o");
    texto = texto.replace(/ú/gi,"u");
    return texto;
}

function eliminarPalabrasRepetidasDosIds(ids1, ids2){
    arraySinRepetidos = [];
    for(var i=0;i<ids1.length;i++){
        if(ids1[i] != 'undefined' && ids1[i] != '' && ids2[i] != 'undefined' && ids2[i] != ''){
            if(habilitarAniadirDosIds(ids1[i], ids2[i], arraySinRepetidos)){
                var objetoIds = new Object();
                objetoIds.id1= ids1[i];
                objetoIds.id2= ids2[i];
                                
                arraySinRepetidos[arraySinRepetidos.length] = objetoIds;
            }
        }
    } 
    return arraySinRepetidos;
}
function habilitarAniadirDosIds(valor1, valor2, arraySinRepetidos){
    retorno = true;
    for(var i=0; i<arraySinRepetidos.length; i++){
        if(arraySinRepetidos[i].id1==valor1 && arraySinRepetidos[i].id2==valor2){
            retorno = false;
        }
    }
    return retorno;
}

module.exports = construc_ranking;