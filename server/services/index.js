'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');
const config = require('../config');
function createToken(user){
    const payload ={
        sub: user._id,
        iat: momento().unix(),//creacion de token
        exp: moment().add(14, 'days').unix(),
    }
    return jwt.encode(payload, config.SECRET_TOKEN)
}

module.exports = createToken;