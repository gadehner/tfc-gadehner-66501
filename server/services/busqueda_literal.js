var busqueda_literal = {};
const mysql = require('mysql');
var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "bdres"
});
var clusterfck = require("tayden-clusterfck");
var shared = require('./shared-variables');
const CANTIDAD_INTERVALOS = shared.CANTIDAD_INTERVALOS;
var CANTIDAD_SENTENCIAS = shared.CANTIDAD_SENTENCIAS;
const valores_discretos = shared.VALORES_DISCRETOS;

busqueda_literal.busqueda_lite = async (palabras_consulta, res, quantity_id, method) =>{
    //obtencion de sentencias
    var query = "SELECT res.numero_expediente id1, res.numero_resolucion id2, res.anio_resolucion, "
            +" res.caratula title, res.texto_resolucion texto"
            +" FROM resoluciones res LEFT JOIN testsresoluciones tr" 
            +" ON res.numero_expediente=tr.numero_expediente" 
            +" AND res.numero_resolucion=tr.numero_resolucion" 
            +" AND res.anio_resolucion=tr.anio_resolucion" 
            +" WHERE tr.idtest='"+quantity_id+"'";  

            //si quantity es null, entonces cambiar la consulta y poner ni like en la nueva
    var conjunto_de_sentencias = (await consultaMysql(query,con)).documentos;
    

    //armado de ranking de acuerdo a cantidad de ocurrencias, discretizacion de relevancia
    var sentencias_y_atributos = [];
    for(var i = 0; i < conjunto_de_sentencias.length; i++){
        //sentencias_y_atributos[sentencias_y_atributos.length]=conjunto_de_sentencias[i];
        var objeto_atributos = new Object();
        objeto_atributos.trazabilidad = 'undefined';
        objeto_atributos.ranking = 0;
        objeto_atributos.calificacion = 'undefined';
        objeto_atributos.posicion = 'undefined';
        objeto_atributos.parametro_de_busqueda = palabras_consulta;
        objeto_atributos.relevancia = 'undefined';
        objeto_atributos.anio_resolucion = conjunto_de_sentencias[i].anio_resolucion;
        objeto_atributos.calificacion = 'undefined';
        objeto_atributos.id1 = conjunto_de_sentencias[i].id1;
        objeto_atributos.id2 = conjunto_de_sentencias[i].id2;
        objeto_atributos.title = conjunto_de_sentencias[i].title;
        objeto_atributos.texto = conjunto_de_sentencias[i].texto;

        sentencias_y_atributos[sentencias_y_atributos.length]=objeto_atributos;
        for(j = 0; j < palabras_consulta.length; j++){
            var count = (conjunto_de_sentencias[i].texto.match(new RegExp(palabras_consulta[j], 'gi')) || []).length
            var count_titulo = (conjunto_de_sentencias[i].title.match(new RegExp(palabras_consulta[j], 'gi')) || []).length
            sentencias_y_atributos[i].ranking += count + count_titulo; 
            //console.log(sentencias_y_atributos[i].ranking);
        }
    }
    //aca ordenar sort
    sentencias_y_atributos.sort((a, b)=> b.ranking - a.ranking);
    if(method==1){
        sentencias_y_atributos = discretizar_relevancia_sintactica(sentencias_y_atributos);
    }
    if(method==6){
        sentencias_y_atributos = discretizar_relevancia_sintactica_con_clustering(sentencias_y_atributos, conjunto_de_sentencias.length);
    }
    
    res.json(sentencias_y_atributos);
}

function discretizar_relevancia_sintactica_con_clustering(arrayFormateado, cantidad_test){
    CANTIDAD_SENTENCIAS = cantidad_test;
    
    var index=-1;
    
            
   
    var values_clustering=[];
    
    for(var i = 0; i < CANTIDAD_SENTENCIAS; i++){
        values_clustering[i]=[arrayFormateado[i].ranking, 0];
    }
    //console.log(values_clustering);
    var limites_escala_relevancia = obtener_escala_relevancia(values_clustering);
    //console.log(limites_escala_relevancia);
    

    for(var i = 0; i < CANTIDAD_SENTENCIAS; i++){
        if(typeof arrayFormateado[i] == 'object'){
            arrayFormateado[i]['posicion'] = i+1;
        }

        
        for(var s=0; s < CANTIDAD_INTERVALOS; s++){
            if(arrayFormateado[i]['ranking']>=(limites_escala_relevancia[s]['min']) && arrayFormateado[i]['ranking']<=(limites_escala_relevancia[s]['max'] )){
                index = s;
            }
        }
        if(index==-1){
            index=4;
        }
        
        arrayFormateado[i]['relevancia'] = valores_discretos[index];  


    }

    return arrayFormateado;
}



function consultaMysql(query_, con){
    return new Promise(function(resolve, reject){
        
        con.query(query_, function (err, result, fields) {
            if (err){ console.log(err); throw err;};
            resolve({documentos: result});
        });
        
    });
}


function habilitarAniadirDosIds(valor1, valor2, arraySinRepetidos){
    retorno = true;
    for(var i=0; i<arraySinRepetidos.length; i++){
        if(arraySinRepetidos[i].id1==valor1 && arraySinRepetidos[i].id2==valor2){
            retorno = false;
        }
    }
    return retorno;
}


function obtener_escala_relevancia(values_clustering){
    var tree = clusterfck.hcluster(values_clustering, "euclidean", "average");
    var escala_relevancia = tree.clusters(CANTIDAD_INTERVALOS);
    var array_intervalos_relevancia=[];
    for(var i = 0; i<escala_relevancia.length;i++){
       var intervalos_relevancia = new Object();
       intervalos_relevancia['min']=obtener_valor_menor(escala_relevancia[i]);
       intervalos_relevancia['max']=obtener_valor_mayor(escala_relevancia[i]);
       array_intervalos_relevancia[i]=intervalos_relevancia;
    }
    console.log(array_intervalos_relevancia);
    array_intervalos_relevancia.sort((a, b)=> a.max - b.max);
    console.log('**************')
    console.log(array_intervalos_relevancia);
    return(array_intervalos_relevancia);
 }
 
 function obtener_valor_menor(vector){
 
    vector=obtener_nuevo_vector(vector);
    val=vector[0];
    for(var i=0; i < vector.length;i++){
       if(vector[i]<val){
          val=vector[i];
       }
    }
 
    return val;
 }
 function obtener_valor_mayor(vector){
 
    vector=obtener_nuevo_vector(vector);
    val=vector[0];
    for(var i=0; i < vector.length;i++){
       if(vector[i]>val){
          val=vector[i];
       }
    }
 
    return val;
 }
 
 function obtener_nuevo_vector(vector){
    var nuevo_vector=[];
    for(var i=0; i<vector.length;i++){
       nuevo_vector[i]=vector[i][0];
    }
 
    return nuevo_vector;
 }
 function discretizar_relevancia_sintactica(sentencias_y_atributos){
    var valor_maximo = sentencias_y_atributos[0].ranking;
    CANTIDAD_SENTENCIAS = sentencias_y_atributos.length;
    if(sentencias_y_atributos.length>10){
        var valor_minimo = sentencias_y_atributos[CANTIDAD_SENTENCIAS-1].ranking;
    }else{
        var valor_minimo = sentencias_y_atributos[auxArr.length-1].ranking;
    }
    var dimension_intervalo = (valor_maximo - valor_minimo)/CANTIDAD_INTERVALOS;
    var index=-1;
  console.log(dimension_intervalo);
    for(var i = 0; i < sentencias_y_atributos.length; i++){
        //if(typeof sentencias_y_atributos[sentencias_y_atributos.length-1] == 'object'){
            sentencias_y_atributos[i]['posicion'] = i+1;
        //}

        for(var s=0; s < 5; s++){
            if(sentencias_y_atributos[i]['ranking']>=(dimension_intervalo * s) && sentencias_y_atributos[i]['ranking']<=(dimension_intervalo * (s+1)) ){
                index = s;
            }
        }
        if(index==-1){
            index=4;
        }
        
        sentencias_y_atributos[i]['relevancia'] = valores_discretos[index];  


    }
    
    return sentencias_y_atributos;
}

module.exports = busqueda_literal;