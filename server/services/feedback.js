var feedback = {};
const { Connection, query } = require('stardog');
const TermBehavior = require('../models/term.behavior');
const shared = require('../services/shared-variables');
const CANTIDAD_INTERVALOS = shared.CANTIDAD_INTERVALOS;
var CANTIDAD_SENTENCIAS = shared.CANTIDAD_SENTENCIAS;
const valores_discretos = shared.VALORES_DISCRETOS;
var consulta_sparql = require('./shared_functions/consulta_sparql');


feedback.feedback = async (req, res) => {//debo pasar method para asi modificar correctamente
    const PONDERACION_MINIMA = shared.PONDERACION_MINIMA;
    const COEFICIENTE_DEBILITAR_FORTALECER = 0.90;
    //el coeficiente se "decrece" en funcion del error...
    //const valores_evaluacion = ['Nada Relevante','PR','SR','R','MR'];
    //valores_discretos


    console.log(req.body);
    var datos_documento = req.body;
    
    var index_sistema_experto = obtener_indice_relevancia(datos_documento.relevancia)+1;
    var index_calificacion_usuario =obtener_indice_relevancia(datos_documento.type)+1;
    var method = datos_documento.method;
    var bd_sparql=null;
    //si es un test, van bds distintas
    if(method==2 || method==3){
        bd_sparql = 'bd_ngd';
    }else{
        if(method==4 || method==5){
            bd_sparql = 'bd_djn';
        }
    }

    var error_documento = (index_calificacion_usuario - index_sistema_experto)/CANTIDAD_INTERVALOS;//sd-so

    var trazabilidad = datos_documento.trazabilidad;
 
    for(var h=0;h<trazabilidad.length;h++){
        for(var i = 0; i < trazabilidad[h].length; i++){
            if(trazabilidad[h][i].termino != datos_documento.parametro_de_busqueda[h]){
                console.log(trazabilidad[h][i].ponderacion_resultante);
                //recuperacion de ponderacion desde sparql dado que por cada thumbup o down se actualiza
                //la bd y los dtos quedan con el antiguo valor
                var termino_ = normalizar_termino_sparql(trazabilidad[h][i].termino);
                            
                var parametro_= normalizar_termino_sparql(datos_documento.parametro_de_busqueda[h]);
                        
                //primero buscar id de cada termino
                //luego buscar ponderacion
                //a continuacion, modificar peso
                var id_termino_= (await consulta_sparql.consultaSparql('select ?s ?p ?o where {:'+termino_+' :id ?o.}', bd_sparql)).resultado[0].o.value;
                var id_parametro_ = (await consulta_sparql.consultaSparql('select ?s ?p ?o where {:'+parametro_+' :id ?o.}', bd_sparql)).resultado[0].o.value;
                var valor_ponderacion = (await consulta_sparql.consultaSparql('select ?s ?p ?o where {:id_'+id_termino_.toString()+' :id_'+id_parametro_.toString()+' ?o.}', bd_sparql)).resultado[0].o.value;


                //var valor_ponderacion = (await consultaSparql('select ?s ?p ?o where {:'+termino_+' :'+parametro_+' ?o.}')).resultado[0].o.value;
                //valor_ponderacion = valor_ponderacion.replace('http://api.stardog.com/', '');
                //valor_ponderacion = parseFloat(valor_ponderacion);
                  
                trazabilidad[h][i].ponderacion_resultante = parseFloat(valor_ponderacion);
                console.log('valor obtenido de sparql: '+trazabilidad[h][i].ponderacion_resultante);


                /*if(datos_documento.type == 'up'){//fortalece relacion
                    trazabilidad[i].ponderacion_resultante = trazabilidad[i].ponderacion_resultante + (((trazabilidad[i].ocurrencia * trazabilidad[i].ponderacion_resultante)/datos_documento.ranking) * COEFICIENTE_DEBILITAR_FORTALECER);
                                                                                                                                
                }else{ //debilita relacion
                    trazabilidad[i].ponderacion_resultante = trazabilidad[i].ponderacion_resultante - (((trazabilidad[i].ocurrencia * trazabilidad[i].ponderacion_resultante)/datos_documento.ranking) * COEFICIENTE_DEBILITAR_FORTALECER);
                }*/
                var coeficiente_participacion = ((trazabilidad[h][i].ocurrencia * trazabilidad[h][i].ponderacion_resultante)/datos_documento.ranking);
                trazabilidad[h][i].ponderacion_resultante = trazabilidad[h][i].ponderacion_resultante + ((error_documento)*(coeficiente_participacion * COEFICIENTE_DEBILITAR_FORTALECER));
                console.log('ponderacion luego de calificacion: '+trazabilidad[h][i].ponderacion_resultante);
                console.log(error_documento);


                if(trazabilidad[h][i].ponderacion_resultante < 0){
                    trazabilidad[h][i].ponderacion_resultante = PONDERACION_MINIMA;
                }
                console.log(trazabilidad[h][i].ponderacion_resultante);
                
                //Se actualizan los pesos en stardog...
                //Volver a revisar ya que los pesos se actualizan con los datos que se
                //recuperan del frontend asi que si se actualiza el peso de una palabra y luego
                //otra, la segunda no se encuentra con los datos anteriores....
                
                var parametro_de_busqueda_ = datos_documento.parametro_de_busqueda[h].replace(/ /g,'_');
                parametro_de_busqueda_ = parametro_de_busqueda_.replace(')','');
                parametro_de_busqueda_ = parametro_de_busqueda_.replace(')','');
                /*await (consultaSparql('delete {?s ?p ?o}'
                                +' insert {?s ?p :'+ trazabilidad[h][i].ponderacion_resultante +'}'
                                +' where {?s ?p ?o . '
                                +' :'+ termino_+' :'+parametro_de_busqueda_+' ?o}'));  */

                await (consulta_sparql.consultaSparql('PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> '
                            +' delete {?s ?p ?o}'
                            +' insert {?s ?p "'+ trazabilidad[h][i].ponderacion_resultante +'"^^xsd:float }'
                            +' where {?s ?p ?o . '
                            +' :id_'+ id_termino_.toString()+' :id_'+id_parametro_.toString()+' ?o}', bd_sparql)); 

                

                ///Comportamiento de ponderaciones en mongodb...
                const term_behavior = new TermBehavior({
                    term: trazabilidad[h][i].termino,
                    ponderacion: trazabilidad[h][i].ponderacion_resultante,
                    date: Date.now(),
                    username: datos_documento.username
                });  
                await term_behavior.save();
                
            }    
        }

    
        
    }
    //console.log(datos_documento);

   
   res.json('ok');
}

feedback.feedback_in_test = async (relevancia_sistema, relevancia_experto, method, trazabilidad, parametro_de_busqueda, valor_ranking_sin_credito)=>{
    const PONDERACION_MINIMA = shared.PONDERACION_MINIMA;
    const COEFICIENTE_DEBILITAR_FORTALECER = 0.9;

    
    var index_sistema_experto = obtener_indice_relevancia(relevancia_sistema)+1;
    var index_calificacion_usuario =obtener_indice_relevancia(relevancia_experto)+1;
  
    var bd_sparql=null;
    //si es un test, van bds distintas
    if(method==2 || method==3){
        bd_sparql = 'bd_ngd_test1';
    }else{
        if(method==4 || method==5){
            bd_sparql = 'bd_djn_test1';
        }
    }

    var error_documento = (index_calificacion_usuario - index_sistema_experto)/CANTIDAD_INTERVALOS;//sd-so

    //var trazabilidad = datos_documento.trazabilidad;
 
    for(var h=0;h<trazabilidad.length;h++){
        for(var i = 0; i < trazabilidad[h].length; i++){

            if(trazabilidad[h][i].termino != parametro_de_busqueda[h]){
                console.log(trazabilidad[h][i].ponderacion_resultante);
                //recuperacion de ponderacion desde sparql dado que por cada thumbup o down se actualiza
                //la bd y los dtos quedan con el antiguo valor
                var termino_ = normalizar_termino_sparql(trazabilidad[h][i].termino);
                            
                var parametro_= normalizar_termino_sparql(parametro_de_busqueda[h]);
                        
                //primero buscar id de cada termino
                //luego buscar ponderacion
                //a continuacion, modificar peso
                var id_termino_= (await consulta_sparql.consultaSparql('select ?s ?p ?o where {:'+termino_+' :id ?o.}', bd_sparql)).resultado[0].o.value;
                var id_parametro_ = (await consulta_sparql.consultaSparql('select ?s ?p ?o where {:'+parametro_+' :id ?o.}', bd_sparql)).resultado[0].o.value;
                var valor_ponderacion = (await consulta_sparql.consultaSparql('select ?s ?p ?o where {:id_'+id_termino_.toString()+' :id_'+id_parametro_.toString()+' ?o.}', bd_sparql)).resultado[0].o.value;


                //var valor_ponderacion = (await consultaSparql('select ?s ?p ?o where {:'+termino_+' :'+parametro_+' ?o.}')).resultado[0].o.value;
                //valor_ponderacion = valor_ponderacion.replace('http://api.stardog.com/', '');
                //valor_ponderacion = parseFloat(valor_ponderacion);
                  
                trazabilidad[h][i].ponderacion_resultante = parseFloat(valor_ponderacion);
                console.log('valor obtenido de sparql: '+trazabilidad[h][i].ponderacion_resultante);


                /*if(datos_documento.type == 'up'){//fortalece relacion
                    trazabilidad[i].ponderacion_resultante = trazabilidad[i].ponderacion_resultante + (((trazabilidad[i].ocurrencia * trazabilidad[i].ponderacion_resultante)/datos_documento.ranking) * COEFICIENTE_DEBILITAR_FORTALECER);
                                                                                                                                
                }else{ //debilita relacion
                    trazabilidad[i].ponderacion_resultante = trazabilidad[i].ponderacion_resultante - (((trazabilidad[i].ocurrencia * trazabilidad[i].ponderacion_resultante)/datos_documento.ranking) * COEFICIENTE_DEBILITAR_FORTALECER);
                }*/
                var coeficiente_participacion = ((trazabilidad[h][i].ocurrencia * trazabilidad[h][i].ponderacion_resultante)/valor_ranking_sin_credito);
                trazabilidad[h][i].ponderacion_resultante = trazabilidad[h][i].ponderacion_resultante + ((error_documento)*(coeficiente_participacion * COEFICIENTE_DEBILITAR_FORTALECER));
                console.log('ponderacion luego de calificacion: '+trazabilidad[h][i].ponderacion_resultante);
                console.log(error_documento);


                if(trazabilidad[h][i].ponderacion_resultante < 0){
                    trazabilidad[h][i].ponderacion_resultante = PONDERACION_MINIMA;
                }
                console.log(trazabilidad[h][i].ponderacion_resultante);
                
                //Se actualizan los pesos en stardog...
                //Volver a revisar ya que los pesos se actualizan con los datos que se
                //recuperan del frontend asi que si se actualiza el peso de una palabra y luego
                //otra, la segunda no se encuentra con los datos anteriores....
                
                var parametro_de_busqueda_ = parametro_de_busqueda[h].replace(/ /g,'_');
                parametro_de_busqueda_ = parametro_de_busqueda_.replace(')','');
                parametro_de_busqueda_ = parametro_de_busqueda_.replace(')','');
                /*await (consultaSparql('delete {?s ?p ?o}'
                                +' insert {?s ?p :'+ trazabilidad[h][i].ponderacion_resultante +'}'
                                +' where {?s ?p ?o . '
                                +' :'+ termino_+' :'+parametro_de_busqueda_+' ?o}'));  */

                await (consulta_sparql.consultaSparql('PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> '
                            +' delete {?s ?p ?o}'
                            +' insert {?s ?p "'+ trazabilidad[h][i].ponderacion_resultante +'"^^xsd:float }'
                            +' where {?s ?p ?o . '
                            +' :id_'+ id_termino_.toString()+' :id_'+id_parametro_.toString()+' ?o}', bd_sparql)); 

                

                ///Comportamiento de ponderaciones en mongodb...
                /*const term_behavior = new TermBehavior({
                    term: trazabilidad[h][i].termino,
                    ponderacion: trazabilidad[h][i].ponderacion_resultante,
                    date: Date.now(),
                    username: datos_documento.username
                });  
                await term_behavior.save();*/
                
            }    
        }

    
        
    }

    console.log("prueba feedback in test");
}


function obtener_indice_relevancia(relevancia){
    var index =-1;
    for(var i = 0; i < 5; i++){
        if(valores_discretos[i]==relevancia){
            index = i;
        }
    }
    return index;
}

function normalizar_termino_sparql(termino_objeto){
    var termino_ = termino_objeto.replace(/ /g, '_');
    termino_ = termino_.replace('(','');
    termino_ = termino_.replace(')','');
    termino_ = termino_.replace('°','');
    termino_ = replaceAll(termino_,'.', '');
    termino_ = replaceAll(termino_,',', '');
    /*if(termino_.length > 40){
        termino_ = termino_.substring(0, 41);
    }*/
    return termino_;
}


function replaceAll( text, busca, reemplaza ){
    while (text.toString().indexOf(busca) != -1)
        text = text.toString().replace(busca,reemplaza);
    return text;
}

module.exports = feedback;