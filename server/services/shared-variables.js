module.exports ={
    /*CANTIDAD_INTERVALOS : 3,
    CANTIDAD_SENTENCIAS : 10,
    VALORES_DISCRETOS : ['Nada Relevante',
                                'Suficientemente Relevante','Muy Relevante'],
    PONDERACION_MAXIMA : 0.80,
    PONDERACION_MINIMA : 0.05,*/

     CANTIDAD_INTERVALOS : 5,
    CANTIDAD_SENTENCIAS : 10,
    VALORES_DISCRETOS : ['Nada Relevante','Poco Relevante',
                                'Suficientemente Relevante','Relevante','Muy Relevante'],
    PONDERACION_MAXIMA : 1,
    PONDERACION_MINIMA : 0.05,
     
}