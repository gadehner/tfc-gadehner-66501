var recup_sentencias ={};

/**
 * coneccion con demas modulos
 */
var consulta_mysql = require('./shared_functions/consulta_mysql');


/**
 * Para poder hacer stemming de palabras al realizar la búsqueda  
 */ 
const natural = require('natural');

recup_sentencias.buscarSentencias_test = (objeto, quantity_id) => {
    var query = "";
    return new Promise(async function (resolve, reject){
        var retorno = [];

        for(var i=0; i<objeto.length; i++){
            /*query =  "SELECT numero_expediente id1, anio_resolucion id2, numero_expediente title, caratula cuerpo, texto_resolucion texto "+
            "FROM `resoluciones` WHERE caratula LIKE '%" + objeto[i] + "%' "+
            "OR texto_resolucion LIKE '%" + objeto[i] + "%'";*/
            
            //objeto[i] = natural.PorterStemmerEs.stem(objeto[i]);
            query = "SELECT res.numero_expediente id1, res.numero_resolucion id2, res.anio_resolucion, "
            +" res.caratula title, res.texto_resolucion texto, rel.descripcion descripcion_rel"
            +" FROM resoluciones res LEFT JOIN testsresoluciones tr" 
            +" ON res.numero_expediente=tr.numero_expediente" 
            +" AND res.numero_resolucion=tr.numero_resolucion" 
            +" AND res.anio_resolucion=tr.anio_resolucion"
            +" INNER JOIN relevancia rel ON rel.id_relevancia=tr.id_relevancia" 
            +" WHERE tr.idtest='"+quantity_id+"'"; 
            //console.log(query);
            var document = await consulta_mysql.consultaMysql(query);
            
            retorno[i]= document.documentos;
            //console.log(document);

        }
        resolve({resultados: retorno});
    });
}

recup_sentencias.buscarSentencias = (objeto) => {
    var query = "";
    return new Promise(async function (resolve, reject){
        var retorno = [];

        for(var i=0; i<objeto.length; i++){
            /*query =  "SELECT numero_expediente id1, anio_resolucion id2, numero_expediente title, caratula cuerpo, texto_resolucion texto "+
            "FROM `resoluciones` WHERE caratula LIKE '%" + objeto[i] + "%' "+
            "OR texto_resolucion LIKE '%" + objeto[i] + "%'";*/
            
            objeto[i] = natural.PorterStemmerEs.stem(objeto[i]);
            //objeto[i]=

            query = "SELECT numero_expediente id1, numero_resolucion id2, anio_resolucion, "+
            "caratula title, texto_resolucion texto FROM `resoluciones` WHERE caratula LIKE '%"+
            objeto[i] + "%' OR texto_resolucion LIKE '%" + objeto[i] + "%'";
            //console.log(query);
            var document = await consulta_mysql.consultaMysql(query);
            //console.log(document);
            retorno[i]= document.documentos;
            //console.log(document);

        }
        resolve({resultados: retorno});
    });
}



module.exports = recup_sentencias;