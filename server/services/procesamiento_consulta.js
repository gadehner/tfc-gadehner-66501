const procesamiento_consulta={};
const SpellChecker = require('simple-spellchecker');


procesamiento_consulta.adecuacion_de_termino = async (palabra) => {
    palabra = eliminar_espacios(palabra);
    palabra = eliminar_caracteres_especiales(palabra);

    //console.log(palabra);
	palabra = await spelling(palabra);
	//console.log(palabra);
    return palabra;
}

function eliminar_espacios(palabra){
    salir = false;
    while(!salir){
        var bandera = false;
        if(palabra[0] == ' '){
            palabra=palabra.substring(1, palabra.length);
            bandera = true;
        }
        if(palabra[palabra.length-1] == ' '){
            palabra=palabra.substring(0, palabra.length-1);
            bandera = true;
        }
        if(!bandera){
            salir = true;
        }
    }
    return palabra;
}


function eliminar_caracteres_especiales(palabra){
    palabra = replaceAll(palabra,'!', '');
    palabra = replaceAll(palabra,'@', '');
    palabra = replaceAll(palabra,';', '');
    palabra = replaceAll(palabra,'#', '');
    palabra = replaceAll(palabra,'~', '');
    palabra = replaceAll(palabra,'`', '');
    palabra = replaceAll(palabra,'°', '');
    palabra = replaceAll(palabra,"'", '');
    palabra = replaceAll(palabra,'"', '');
    return palabra;
}


function spelling(palabra){
	return new Promise(function(resolve, reject){
		SpellChecker.getDictionary("es-ES", function(err, dictionary) {
		    if(!err) {
			var misspelled = ! dictionary.spellCheck(palabra);
			// si es true, entonces hay error o no encontro la palabra
			console.log(misspelled);
			if(misspelled) {
			//si hay error, entonces busca sugerencias, puede encontrarlas o no
			    var suggestions = dictionary.getSuggestions(palabra);
			    console.log(suggestions);
			    if(suggestions.length > 0){	
			    	palabra = suggestions[0];
			    }
			    
			}
			resolve(palabra);
		    }
		});    		
	});
}

function replaceAll( text, busca, reemplaza ){
    while (text.toString().indexOf(busca) != -1)
        text = text.toString().replace(busca,reemplaza);
    return text;
}

module.exports = procesamiento_consulta;