const mongoose = require('mongoose');

const URI = 'mongodb://localhost/mean-documents'; //mongodb://localhost/mean-documents'

mongoose.connect(URI, { useNewUrlParser: true })//antes era sin { useNewUrlParser: true }
    .then(db => console.log('Base de datos conectada'))
    .catch(err=> console.error(err));

mongoose.set('useCreateIndex', true);

module.exports = mongoose;