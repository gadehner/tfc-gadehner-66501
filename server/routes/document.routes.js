const express = require('express');
const router = express.Router();

const document = require('../controllers/document.controller');

//para uso como rest api
//router.get('/', document.getDocuments);//get para obtener
//router.post('/', document.createDocument);//para guardar
//router.get('/:id', document.getDocument);
//router.put('/:id', document.editDocument);//para actualizar datos
//router.delete('/:id', document.deleteDocument)
router.get('/:title', document.searchDocument);
router.put('/:params', document.testModel);
//router.get('/:title', document.feedback)
router.post('/:title', document.feedback);


module.exports = router;