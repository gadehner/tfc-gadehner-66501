const express = require('express');
const router = express.Router();

const data_test = require('../controllers/data_test.controller');

//para uso como rest api

router.post('/crear/', data_test.createDataTest);
router.post('/crearbehavior/', data_test.createBehaviorTest);
router.get('/', data_test.getCorpus);//get para obtener
router.get('/:id', data_test.getQuantitySentencias);



//router.post('/', user.login);



module.exports = router;
