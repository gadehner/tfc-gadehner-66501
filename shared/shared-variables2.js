module.exports ={
    CANTIDAD_INTERVALOS : 5,
    CANTIDAD_SENTENCIAS : 10,
    VALORES_DISCRETOS : ['Nada Relevante','Poco Relevante',
                                'Suficientemente Relevante','Relevante','Muy Relevante']
}